import pytest


@pytest.mark.skip(reason="Pending to implement")
def test_binary_data():
    """Test events with non-ascii, non-utf-8 binary data"""


@pytest.mark.skip(reason="Pending to implement")
def test_continuous_backup_of_topics():
    """Test continuous backing up of data"""


@pytest.mark.skip(reason="Pending to implement")
def test_discover_topic_changes():
    """Detect when topics are added or removed, or new partitions are added"""


@pytest.mark.skip(reason="Pending to implement")
def test_compacted_topics():
    """Test backup and restore of compacted topics"""


@pytest.mark.skip(reason="Pending to implement")
def test_efficient_serialization():
    """Test efficient approach, format and algorithms for storing backup data"""


@pytest.mark.skip(reason="Pending to implement")
def test_chain_diverges():
    """Test backups and validation works correctly when chain of blocks diverges"""


@pytest.mark.skip(reason="Pending to implement")
def test_orphan_blocks_detected_and_deleted():
    """Test behaviour when orphan blocks exists"""


# Out of scope for v1


@pytest.mark.skip(reason="Out of scope for now")
def test_kafka_transactions():
    """Test backup and restore of topics where tx are being used"""


@pytest.mark.skip(reason="Out of scope for now")
def test_backup_consumer_group_offsets():
    """Test backing up consumer group offsets"""


@pytest.mark.skip(reason="Out of scope for now")
def test_restore_consumer_group_offsets():
    """
    Test restoring of consumer group offsets,
    including adjusting difference for partition offset of new restored topic.
    """


@pytest.mark.skip(reason="Out of scope for now")
def test_kip_516():
    """
    If topic is recreated with same name, behaviour is undefined.
    """
