from unittest.mock import patch

import pytest

from kafkup.configuration import Configuration
from kafkup.kafka_client import KafkaClient
from kafkup.kafka_client import PartitionMetadata
from kafkup.kafka_client import TopicMetadata
from kafkup.kafka_client import TopicsMetadata
from kafkup.partition_discovery import MetadataUpdate
from kafkup.partition_discovery import PartitionDiscoveryBase
from kafkup.partition_discovery import StopProcessException


@pytest.mark.parametrize("builder_method", ["as_process", "as_thread"])
def test_initial_update_marked_as_changed(builder_method, configuration: Configuration):
    with patch.object(KafkaClient, "get_topics_metadata") as mock_get_topics_metadata:
        mock_get_topics_metadata.side_effect = [
            TopicsMetadata(
                topics=[
                    TopicMetadata(
                        topic="some-topic",
                        partitions_metadata=[
                            PartitionMetadata(topic="some-topic", partition=0, offset=None),
                            PartitionMetadata(topic="some-topic", partition=1, offset=None),
                        ],
                    )
                ]
            ),
            StopProcessException(),
        ]
        partition_discovery_class = getattr(PartitionDiscoveryBase, builder_method)()
        process = partition_discovery_class(
            daemon=True,
            sleep_between_poll=0.05,
            configuration=configuration,
        )
        process.start()

        update: MetadataUpdate = process.metadata_update_queue.get(timeout=1)
        assert update.changed
        assert {_.topic for _ in update.partitions_metadata} == {"some-topic"}
        assert {_.partition for _ in update.partitions_metadata} == {0, 1}


@pytest.mark.parametrize("builder_method", ["as_process", "as_thread"])
def test_initial_update_empty(builder_method, configuration: Configuration):
    with patch.object(KafkaClient, "get_topics_metadata") as mock_get_topics_metadata:
        mock_get_topics_metadata.side_effect = [
            TopicsMetadata(topics=[]),
            StopProcessException(),
        ]
        partition_discovery_class = getattr(PartitionDiscoveryBase, builder_method)()
        process = partition_discovery_class(daemon=True, sleep_between_poll=0.05, configuration=configuration)
        process.start()

        update: MetadataUpdate = process.metadata_update_queue.get(timeout=1)
        assert update.changed
        assert not update.partitions_metadata


@pytest.mark.parametrize("builder_method", ["as_process", "as_thread"])
def test_third_update_marked_as_changed(builder_method, configuration: Configuration):
    with patch.object(KafkaClient, "get_topics_metadata") as mock_get_topics_metadata:
        mock_get_topics_metadata.side_effect = [
            TopicsMetadata(
                topics=[
                    TopicMetadata(
                        topic="some-topic",
                        partitions_metadata=[
                            PartitionMetadata(topic="some-topic", partition=0, offset=None),
                            PartitionMetadata(topic="some-topic", partition=1, offset=None),
                        ],
                    ),
                ]
            ),
            TopicsMetadata(
                topics=[
                    TopicMetadata(
                        topic="some-topic",
                        partitions_metadata=[
                            PartitionMetadata(topic="some-topic", partition=0, offset=None),
                            PartitionMetadata(topic="some-topic", partition=1, offset=None),
                        ],
                    ),
                ]
            ),
            TopicsMetadata(
                topics=[
                    TopicMetadata(
                        topic="some-topic",
                        partitions_metadata=[
                            PartitionMetadata(topic="some-topic", partition=0, offset=None),
                            PartitionMetadata(topic="some-topic", partition=1, offset=None),
                        ],
                    ),
                    TopicMetadata(
                        topic="this-is-a-different-topic",
                        partitions_metadata=[
                            PartitionMetadata(topic="this-is-a-different-topic", partition=0, offset=None),
                        ],
                    ),
                ]
            ),
            StopProcessException(),
        ]
        partition_discovery_class = getattr(PartitionDiscoveryBase, builder_method)()
        process = partition_discovery_class(
            daemon=True,
            sleep_between_poll=0.05,
            configuration=configuration,
        )
        process.start()

        update: MetadataUpdate = process.metadata_update_queue.get(timeout=1)
        assert update.changed
        assert {_.topic for _ in update.partitions_metadata} == {"some-topic"}
        update = process.metadata_update_queue.get(timeout=1)
        assert not update.changed
        assert {_.topic for _ in update.partitions_metadata} == {"some-topic"}
        update = process.metadata_update_queue.get(timeout=1)
        assert update.changed
        assert {_.topic for _ in update.partitions_metadata} == {"some-topic", "this-is-a-different-topic"}
