from coverage.html import os

from kafkup.block import Block
from kafkup.block import BlockData
from kafkup.block import BlockMetadata
from kafkup.block_builder import InMemoryBlockBuilder
from kafkup.configuration import Configuration
from kafkup.pretty_print import PrettyPrinter
from tests.kafka_mock import Scenario


def test_pretty_print_level_disabled(random_block, configuration: Configuration):
    block: Block = random_block()
    configuration = configuration.with_overrides(pretty_print_level=Configuration.PRETTY_PRINT_LEVEL_DISABLED)
    printer = PrettyPrinter(configuration=configuration)
    assert isinstance(printer.pretty_print_data(block.data), str)
    assert isinstance(printer.pretty_print_metadata(block.metadata), str)
    assert isinstance(printer.log_data(block.data), BlockData)
    assert isinstance(printer.log_metadata(block.metadata), BlockMetadata)


def test_pretty_print_level_enabled(random_block, configuration: Configuration):
    block: Block = random_block()
    configuration = configuration.with_overrides(pretty_print_level=Configuration.PRETTY_PRINT_LEVEL_ENABLED)
    printer = PrettyPrinter(configuration=configuration)
    assert isinstance(printer.pretty_print_data(block.data), str)
    assert isinstance(printer.pretty_print_metadata(block.metadata), str)
    assert isinstance(printer.log_data(block.data), str)
    assert isinstance(printer.log_metadata(block.metadata), str)


def test_pretty_print_level_detailed(random_block, configuration: Configuration):
    block: Block = random_block()
    configuration = configuration.with_overrides(pretty_print_level=Configuration.PRETTY_PRINT_LEVEL_DETAILED)
    printer = PrettyPrinter(configuration=configuration)
    assert isinstance(printer.pretty_print_data(block.data), str)
    assert isinstance(printer.pretty_print_metadata(block.metadata), str)
    assert isinstance(printer.log_data(block.data), str)
    assert isinstance(printer.log_metadata(block.metadata), str)


def test_log_real_block(configuration: Configuration):
    """
    To use other level, use env variable:
    KAFKUP_PRETTY_PRINT_LEVEL
    """
    pretty_print_level = int(os.environ.get("KAFKUP_PRETTY_PRINT_LEVEL", "0"))  # Mimics real usage
    configuration = configuration.with_overrides(max_messages_per_block=20, pretty_print_level=pretty_print_level)
    builder = InMemoryBlockBuilder(configuration=configuration)
    scenario = Scenario.create(num_messages=20)
    block = None
    for item in scenario.all_events:
        block = builder.handle_event(Scenario.expected_when_consumed(item, "topic-abc"))

    assert block is not None

    printer = PrettyPrinter(configuration=configuration)
    print("Data: %s" % printer.log_data(block.data))
    print("Metadata: %s" % printer.log_metadata(block.metadata))
