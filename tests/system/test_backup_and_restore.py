import copy
import logging
import os
import time

import pytest
from confluent_kafka import Consumer
from confluent_kafka import Producer

from kafkup import partition_reader
from kafkup.block import KAFKA_MESSAGE_TUPLE_HEADERS
from kafkup.block import KAFKA_MESSAGE_TUPLE_KEY
from kafkup.block import KAFKA_MESSAGE_TUPLE_PARTITION
from kafkup.block import KAFKA_MESSAGE_TUPLE_TIMESTAMP
from kafkup.block import KAFKA_MESSAGE_TUPLE_VALUE
from kafkup.block_builder import InMemoryBlockBuilder
from kafkup.block_storage import S3BlockStorage
from kafkup.configuration import Configuration
from tests.conftest import kafka_required
from tests.conftest import minio_required
from tests.kafka_mock import Scenario

logger = logging.getLogger(__name__)


@pytest.mark.skipif(os.environ.get("RUN_SYSTEM_TESTS", "true") == "false", reason="System tests disabled")
@kafka_required
@minio_required
def test_functional(
    topic: str,
    populator,
    kafka_consumer: Consumer,
    kafka_producer: Producer,
    create_topic,
    kafkacat_dump,
    configuration: Configuration,
):
    # setup topics
    scenario = Scenario.create(num_messages=1000)
    configuration = configuration.with_overrides(max_messages_per_block=100)
    partitions = populator(topic, scenario)

    # launch process
    consumer = partition_reader.PartitionReaderBase.as_thread()(
        daemon=True,
        configuration=configuration,
        partitions=partitions,
        block_builder=InMemoryBlockBuilder(configuration=configuration),
        block_storage=S3BlockStorage(configuration=configuration),
    )
    consumer.start()

    block_storage = S3BlockStorage(configuration=configuration)
    all_metadata = []
    for _ in range(20):
        all_metadata = block_storage.get_all_blocks_metadata()
        if len(all_metadata) != 10:
            # Not ready
            time.sleep(1.0)
            continue

    assert len(all_metadata) == 10, "Backup didn't finished, giving up..."

    # All good, let's restore it
    def _delivery_report(err, msg):
        if err is not None:
            raise Exception(f"Message not delivered: {err}")

    target_topic = create_topic()
    data_blocks = [block_storage.download_data(_) for _ in all_metadata]
    for data in data_blocks:
        for restored_event in data.events:
            if restored_event[KAFKA_MESSAGE_TUPLE_HEADERS]:
                headers = [(_[0], _[1]) for _ in restored_event[KAFKA_MESSAGE_TUPLE_HEADERS]]  # type: ignore
                kafka_producer.produce(
                    callback=_delivery_report,
                    topic=target_topic,
                    partition=restored_event[KAFKA_MESSAGE_TUPLE_PARTITION],
                    value=restored_event[KAFKA_MESSAGE_TUPLE_VALUE],
                    key=restored_event[KAFKA_MESSAGE_TUPLE_KEY],
                    headers=headers,
                    timestamp=restored_event[KAFKA_MESSAGE_TUPLE_TIMESTAMP][1],  # type: ignore
                )
            else:
                kafka_producer.produce(
                    callback=_delivery_report,
                    topic=target_topic,
                    partition=restored_event[KAFKA_MESSAGE_TUPLE_PARTITION],
                    value=restored_event[KAFKA_MESSAGE_TUPLE_VALUE],
                    key=restored_event[KAFKA_MESSAGE_TUPLE_KEY],
                    timestamp=restored_event[KAFKA_MESSAGE_TUPLE_TIMESTAMP][1],  # type: ignore
                )
        kafka_producer.flush()
    kafka_producer.flush()

    dump_source = kafkacat_dump(topic)
    dump_target = kafkacat_dump(target_topic)

    assert len(dump_source) == len(dump_target)
    # `kafkacat_dump` dumps a partition at a time, so we can compare one by one
    for event_dump_source, event_dump_target in zip(dump_source, dump_target):
        # assert event_dump_source == event_dump_target
        assert clean(event_dump_source) == clean(event_dump_target)


def clean(dump_event: dict):
    # {
    #     "topic": "test-5b9adfb3-803a-4333-a536-3645e7a2c231",
    #     "partition": 1,
    #     "offset": 3,
    #     "tstype": "create",
    #     "ts": 1688648612587,
    #     "broker": -1,
    #     "headers": [
    #         "key-2", "7dc15ed8-a949-4088-b7d0-fbc7113f5f0e",
    #         "key-2", "23654843-9ad3-4a26-b31c-ba605aeaf8c3",
    #         "key-2", "675a140a-5353-4f24-89ef-ef58a783ee16",
    #         "key-1", "7af65977-d552-4f5f-84ff-0fe8defb1dd5",
    #         "key-3", "8e5dfeb0-016f-4c0a-b443-ba43ff0babe0"
    #     ],
    #     "key": "office:1",
    #     "payload": "{\"account_id\": 49, \"num\": 5}"
    # }
    cleaned_event = copy.deepcopy(dump_event)
    assert set(cleaned_event.keys()) <= {
        "topic",
        "partition",
        "offset",
        "tstype",
        "ts",
        "broker",
        "headers",
        "key",
        "payload",
    }
    # remove data that always will differ
    del cleaned_event["topic"]
    del cleaned_event["broker"]
    # we can't set offset, it will be different
    # TODO: test with topics with different offsets! Because we use always new topics, we allways start at `0`
    # for both topics, source and target
    del cleaned_event["offset"]
    # probably we won't be able to restore tstype
    del cleaned_event["tstype"]
    # let's make sure we always have `headers`
    if "headers" not in cleaned_event:
        cleaned_event["headers"] = []

    assert set(cleaned_event.keys()) == {"partition", "key", "payload", "headers", "ts"}
    return cleaned_event
