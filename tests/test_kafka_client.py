import pytest
from confluent_kafka import OFFSET_BEGINNING

from kafkup.block import BlockMetadata
from kafkup.block import PartitionOffset
from kafkup.block import TopicPartition
from kafkup.kafka_client import KafkaClient


@pytest.mark.parametrize(
    "desc,partitions,metadata_end,expected",
    [
        # ---
        [
            "without metadata",
            [
                # partitions
                TopicPartition(topic="topic-a", partition=0),
                TopicPartition(topic="topic-a", partition=1),
            ],
            # metadata_end
            None,
            [
                # expected
                PartitionOffset(topic="topic-a", partition=0, offset=OFFSET_BEGINNING),
                PartitionOffset(topic="topic-a", partition=1, offset=OFFSET_BEGINNING),
            ],
        ],
        # ---
        [
            "metadata for each partition",
            [
                # partitions
                TopicPartition(topic="topic-a", partition=0),
                TopicPartition(topic="topic-a", partition=1),
            ],
            {
                # metadata_end
                TopicPartition(topic="topic-a", partition=0): 123,
                TopicPartition(topic="topic-a", partition=1): 456,
            },
            [
                # expected
                PartitionOffset(topic="topic-a", partition=0, offset=123),
                PartitionOffset(topic="topic-a", partition=1, offset=456),
            ],
        ],
        # ---
        [
            "metadata for some partition",
            [
                # partitions
                TopicPartition(topic="topic-a", partition=0),
                TopicPartition(topic="topic-a", partition=1),
            ],
            {
                # metadata_end
                TopicPartition(topic="topic-a", partition=0): 123,
            },
            [
                # expected
                PartitionOffset(topic="topic-a", partition=0, offset=123),
                PartitionOffset(topic="topic-a", partition=1, offset=OFFSET_BEGINNING),
            ],
        ],
        # ---
        [
            "more metadata than partitions",
            [
                # partitions
                TopicPartition(topic="topic-a", partition=0),
            ],
            {
                # metadata_end
                TopicPartition(topic="topic-a", partition=0): 123,
                TopicPartition(topic="topic-a", partition=1): 456,
            },
            [
                # expected
                PartitionOffset(topic="topic-a", partition=0, offset=123),
            ],
        ],
    ],
)
def test_build_assign_partitions_list(desc, partitions, metadata_end, expected):
    if metadata_end is None:
        last_block_metadata = None
    else:
        last_block_metadata = BlockMetadata(block_number=0, counters={}, start={}, end=metadata_end)
    assign_partitions_list = KafkaClient.build_assign_partitions_list(partitions, last_block_metadata)
    assert assign_partitions_list == expected
