from __future__ import annotations

import timeit

import pytest

from tests.benchmarks import Config
from tests.benchmarks import config  # noqa


@pytest.mark.skip
class TestTimeitVsProfile:
    """
    Result: both methods are susceptible to variation in speed of CPU

    Max cpu freq:

    tests/benchmarks.py::TestTimeitVsProfile::test_timeit result=0.4510092029995576

       ncalls  tottime  percall  cumtime  percall filename:lineno(function)
            1    0.000    0.000    2.539    2.539 <string>:1(<module>)
          250    2.539    0.010    2.539    0.010 benchmarks.py:337(do_calculation)
            1    0.000    0.000    2.539    2.539 benchmarks.py:352(mimic_timeit)
            1    0.000    0.000    2.539    2.539 {built-in method builtins.exec}
            1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}

    Min cpu freq:

    tests/benchmarks.py::TestTimeitVsProfile::test_timeit result=1.6522099639987573

       ncalls  tottime  percall  cumtime  percall filename:lineno(function)
            1    0.000    0.000    9.136    9.136 <string>:1(<module>)
          250    9.135    0.037    9.135    0.037 benchmarks.py:337(do_calculation)
            1    0.001    0.001    9.136    9.136 benchmarks.py:352(mimic_timeit)
            1    0.000    0.000    9.136    9.136 {built-in method builtins.exec}
            1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
    """

    @classmethod
    def do_calculation(cls, max_value: int):
        assert max_value >= 0
        cur = 0.0
        while cur < max_value:
            cur += 0.0001

    def test_timeit(self, config: Config):  # noqa: F811
        bench = timeit.Timer(stmt="self.do_calculation(20)", globals=locals())
        result = min(bench.repeat(repeat=config.timeit_repeat, number=config.timeit_number))
        print(f"result={result}")

    def test_profile(self, config: Config):  # noqa: F811
        import cProfile

        def mimic_timeit():
            for i in range(config.timeit_repeat):
                for j in range(config.timeit_number):
                    self.do_calculation(20)

        cProfile.runctx("mimic_timeit()", locals=locals(), globals=globals())
