import multiprocessing
import time
from multiprocessing import Queue
from random import randint


class MyProcess(multiprocessing.Process):
    def __init__(
        self,
        the_queue: Queue,
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.the_queue = the_queue

    def run(self):
        while True:
            self.the_queue.put(randint(1000, 9999))
            time.sleep(1)


def test_parent_process_can_use_queue_on_instance():
    proc = MyProcess(the_queue=multiprocessing.Queue(), daemon=True)
    proc.start()
    value = proc.the_queue.get(timeout=2.0)
    # ^^^ this is what we want to test. It worked :)
    assert isinstance(value, int)
    assert 1000 <= value <= 9999
