import dataclasses
import itertools
import pickle
import tempfile
from random import randint
from uuid import uuid4


@dataclasses.dataclass(frozen=True)
class _Event:
    topic: str
    partition: int
    offset: int
    key: bytes
    value: bytes
    headers: list[tuple[str, bytes]] | None

    @classmethod
    def random(cls):
        return _Event(
            topic=f"topic-{randint(0, 9999)}",
            key=f"pk-{randint(0, 9999)}".encode(),
            value=str(uuid4()).encode("utf-8"),
            partition=randint(0, 9),
            offset=randint(0, 9999),
            headers=[
                ("header-key-1", b"header-value-1"),
                ("header-key-2", b"header-value-2"),
            ],
        )


def test_streaming_serialization():
    fp = tempfile.NamedTemporaryFile()
    pickler_writer = pickle.Pickler(fp)
    events = [_Event.random() for _ in range(10)]
    for event in events:
        pickler_writer.dump(event)

    fp.flush()

    deserialized_events = []
    with open(fp.name, "br") as serialized:
        pickler_reader = pickle.Unpickler(serialized)
        try:
            while True:
                deserialized_events.append(pickler_reader.load())
        except EOFError:
            pass

    assert len(events) == len(deserialized_events)

    for original, deserialized in itertools.zip_longest(events, deserialized_events):
        assert original == deserialized
