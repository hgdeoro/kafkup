import io
import pickle
from collections.abc import Iterator
from uuid import uuid4

import pytest


def get_values() -> Iterator[dict]:
    """
    We want to test the case when we get an element at a time... we want to
    simulate a case when we receive the data to serialize one item at a time.
    """
    for _ in range(10):
        yield {"key": _, "value": f"value-{_}"}


def test(s3_bucket):
    s3_key = f"test_blockio_incremental_serialize_and_upload/{uuid4()}"

    # serialize and upload
    block_data_file_obj = io.BytesIO()
    for value in get_values():
        pickle.dump(value, file=block_data_file_obj)
    block_data_file_obj.seek(0)
    s3_bucket.upload_fileobj(block_data_file_obj, s3_key)

    # download and compare
    download_buffer = io.BytesIO()
    s3_bucket.download_fileobj(s3_key, download_buffer)
    download_buffer.seek(0)

    for expected in get_values():
        assert expected == pickle.load(file=download_buffer)

    with pytest.raises(EOFError):
        pickle.load(file=download_buffer)
