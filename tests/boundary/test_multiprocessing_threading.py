from __future__ import annotations

import abc
import multiprocessing
import queue
import threading
from random import randint
from typing import Self
from typing import TypeAlias
from typing import Union

import pytest

"""
These are the boundary tests, created initially to see if this was possible to do,
and to evaluate if this makes sense. Real implementation and their tests are in a
different modules.
"""


_XQueue: TypeAlias = Union[multiprocessing.Queue, queue.Queue]


class _ThreadOrProcess(abc.ABC):
    @abc.abstractmethod
    def _new_queue(self) -> _XQueue:
        ...

    def start(self):
        return super().start()

    def join(self, timeout=None):
        return super().join(timeout)

    def is_alive(self):
        return super().is_alive()

    @classmethod
    def as_thread(cls) -> type[Self]:
        return type(
            f"{getattr(cls, 'SUBCLASS_BASENAME')}Thread",
            (
                cls,
                _ThreadMixin,
            ),
            {},
        )

    @classmethod
    def as_process(cls) -> type[Self]:
        return type(
            f"{getattr(cls, 'SUBCLASS_BASENAME')}Process",
            (
                cls,
                _ProcessMixin,
            ),
            {},
        )


class _ThreadMixin(threading.Thread, _ThreadOrProcess):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _new_queue(self) -> _XQueue:
        return queue.Queue(maxsize=1)


class _ProcessMixin(multiprocessing.Process, _ThreadOrProcess):
    def _new_queue(self) -> _XQueue:
        return multiprocessing.Queue(maxsize=1)


class SomeBusinessLogic(_ThreadOrProcess, abc.ABC):
    SUBCLASS_BASENAME = "SomeBusinessLogic"

    def __init__(self, count: int, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._count: int = count
        self._result_queue: _XQueue = self._new_queue()

    @property
    def result_queue(self) -> _XQueue:
        return self._result_queue

    def run(self):
        for _ in range(self._count):
            self._result_queue.put({"count": self._count, "generated_value": randint(0, 1_000_000)})


def test_thread_type():
    thread_impl = SomeBusinessLogic.as_thread()
    assert issubclass(thread_impl, SomeBusinessLogic)
    assert issubclass(thread_impl, _ThreadMixin)
    assert not issubclass(thread_impl, _ProcessMixin)


def test_process_type():
    thread_impl = SomeBusinessLogic.as_process()
    assert issubclass(thread_impl, SomeBusinessLogic)
    assert not issubclass(thread_impl, _ThreadMixin)
    assert issubclass(thread_impl, _ProcessMixin)


def test_thread_instance_from_metaclass():
    thread_class = SomeBusinessLogic.as_thread()
    instance = thread_class(count=123, daemon=True)
    assert not instance.is_alive()
    assert instance.native_id is None
    with pytest.raises(AttributeError):
        assert instance.pid is None
    assert instance.__class__.__name__ == "SomeBusinessLogicThread"


def test_process_instance_from_metaclass():
    process_class = SomeBusinessLogic.as_process()
    instance = process_class(count=123, daemon=True)
    assert not instance.is_alive()
    assert instance.pid is None
    with pytest.raises(AttributeError):
        assert instance.native_id is None
    assert instance.__class__.__name__ == "SomeBusinessLogicProcess"


def test_run_thread():
    thread_class = SomeBusinessLogic.as_thread()
    instance = thread_class(count=3, daemon=True)
    instance.start()
    assert "generated_value" in instance.result_queue.get(timeout=1.0)
    assert instance.is_alive()
    assert "generated_value" in instance.result_queue.get(timeout=1.0)
    assert "generated_value" in instance.result_queue.get(timeout=1.0)
    instance.join(timeout=5.0)


def test_run_process():
    thread_class = SomeBusinessLogic.as_process()
    instance = thread_class(count=3, daemon=True)
    instance.start()
    assert "generated_value" in instance.result_queue.get(timeout=1.0)
    assert instance.is_alive()
    assert "generated_value" in instance.result_queue.get(timeout=1.0)
    assert "generated_value" in instance.result_queue.get(timeout=1.0)
    instance.join(timeout=5.0)
