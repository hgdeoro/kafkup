from confluent_kafka import OFFSET_BEGINNING
from confluent_kafka import TopicPartition


def test_topic_partition_expected_comparisons():
    assert TopicPartition(topic="topic-abc", partition=0) == TopicPartition(topic="topic-abc", partition=0)
    assert TopicPartition(topic="topic-abc", partition=0) != TopicPartition(topic="topic-abc", partition=1)
    assert TopicPartition(topic="topic-xyz", partition=0) != TopicPartition(topic="topic-abc", partition=0)
    assert TopicPartition("topic-abc", 0) == TopicPartition("topic-abc", 0)
    assert TopicPartition("topic-abc", 0) != TopicPartition("topic-abc", 1)
    assert TopicPartition("topic-abc", 0) != TopicPartition("topic-xyz", 0)


def test_topic_partition_unexpected_comparisons():
    assert TopicPartition("abc", 0) == TopicPartition("abc", 0)
    assert TopicPartition("abc", 0) == TopicPartition("abc", 0, offset=OFFSET_BEGINNING)
    assert TopicPartition("abc", 0) == TopicPartition("abc", 0, offset=0)
    assert TopicPartition("abc", 0) == TopicPartition("abc", 0, offset=123)
    assert TopicPartition("abc", 0, offset=OFFSET_BEGINNING) == TopicPartition("abc", 0, offset=OFFSET_BEGINNING)
    assert TopicPartition("abc", 0, offset=OFFSET_BEGINNING) == TopicPartition("abc", 0, offset=0)
    assert TopicPartition("abc", 0, offset=OFFSET_BEGINNING) == TopicPartition("abc", 0, offset=123)
