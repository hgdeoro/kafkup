# Directories

Tests in `tests` (this directory) are close to unittest, they can be tested without dependencies.

Tests in `tests.tests_functional` use and require Kafka and/or Minio to be run.

Tests in `tests.tests_boundary` are boundary tests.

# CLI

You will need a configuration file, for example:

    $ cat > config.json<<EOF
    {
        "bucket": "kafkup",
        "backup_name": "sample",
        "kafka_config": {
            "bootstrap.servers": "localhost:9092",
            "group.id": "group-id-from-config"
        }
    }
    EOF

* backup_name can be overwritten via env variable `KAFKUP_BACKUP_NAME`
* log level can be overwritten via env variable `KAFKUP_LOGGING_LEVEL`
* objects dumped in logs can be pretty-formatted: `KAFKUP_PRETTY_PRINT_LEVEL=1` or
  `KAFKUP_PRETTY_PRINT_LEVEL=2` for even more detailed dumps.


# Launch the backuper proces

To launch the main process:

    env \
        KAFKUP_LOGGING_LEVEL=info \
        KAFKUP_BACKUP_NAME=from-cli-main-$(date +%s) \
        KAFKUP_CONFIGURATION_FILE=config.json \
            python3 -m kafkup.main

# Launch specific processes to test specific components

There are some utilities to help development in `kafkup.cli`

### Create topic and populate with random messages: `kafkup.cli.synthetic_create_and_populate`

To create and populate a new topic use `tests/cli/create_and_populate_partition.py`:

    env \
        KAFKUP_CONFIGURATION_FILE=config.json \
        WAIT_BETWEEN_PRODUCE=0.1 \
            python3 -m kafkup.cli.synthetic_create_and_populate

    INFO:__main__:Creating new topic=test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28 with config={}
    INFO:__main__:New topic found, continuing... topic=test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28
    INFO:__main__:Start population of topic test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28...
    INFO:__main__:[t=792597] Message delivered. Total=15002. Throughput=14827/sec. msg=<cimpl.Message object at 0x7f7af1db5640>, topic=test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28, offset=12014
    INFO:__main__:[t=792596] Message delivered. Total=15002. Throughput=14716/sec. msg=<cimpl.Message object at 0x7f7af1db5640>, topic=test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28, offset=24064
    INFO:__main__:[t=792595] Message delivered. Total=15002. Throughput=14700/sec. msg=<cimpl.Message object at 0x7f7af1db5640>, topic=test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28, offset=23973
    INFO:__main__:[t=792601] Message delivered. Total=15002. Throughput=14625/sec. msg=<cimpl.Message object at 0x7f7af1db5640>, topic=test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28, offset=12203


To generate bigger messages with binary data use `DATA_GENERATOR_FUNC`:

    env \
        KAFKUP_CONFIGURATION_FILE=config.json \
        DATA_GENERATOR_FUNC=generate_random_big_binary_payload_to_produce \
            python3 -m kafkup.cli.synthetic_create_and_populate

### Backup a partition: `kafkup.cli.partition_reader_backup_topics`

To backup specific partitions, use `kafkup.cli.partition_reader_backup_topics` and set `TOPIC_PARTITIONS`:

    export TOPIC=test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28  # created by 'kafkup.cli.synthetic_create_and_populate'
    env \
        KAFKUP_CONFIGURATION_FILE=config.json \
        TOPIC_PARTITIONS=$TOPIC:0,$TOPIC:1,$TOPIC:2 \
        KAFKUP_BACKUP_NAME=from-cli-$(date +%s) \
            python3 -m kafkup.cli.partition_reader_backup_topics

    Using TOPIC_PARTITIONS=test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28:0,test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28:1,test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28:2
    INFO:root:Waiting for result...
    INFO:kafkup.block_storage.S3Client:get_all_keys_for_backup(): getting list of files for from-cli-1689928737
    INFO:kafkup.block_storage.S3Client:get_all_keys_for_backup(): Done. Got 0 files
    INFO:kafkup.partition_reader.PartitionReaderProcess:There is no last_block_metadata
    INFO:kafkup.kafka_client.KafkaClient:build_assign_partitions_list() topic=test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28 partition=0 offset=-2
    INFO:kafkup.kafka_client.KafkaClient:build_assign_partitions_list() topic=test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28 partition=1 offset=-2
    INFO:kafkup.kafka_client.KafkaClient:build_assign_partitions_list() topic=test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28 partition=2 offset=-2
    INFO:kafkup.partition_reader.PartitionReaderProcess:Will assign consumer to offsets: [PartitionOffset(topic='test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28', partition=0, offset=-2), PartitionOffset(topic='test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28', partition=1, offset=-2), PartitionOffset(topic='test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28', partition=2, offset=-2)]
    INFO:kafkup.partition_reader.PartitionReaderProcess:New block finished: BlockMetadata(block_number=0, start={}, end={TopicPartition(topic='test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28', partition=2): 99}, counters={TopicPartition(topic='test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28', partition=2): 100})
    INFO:root:Waiting for result...
    INFO:root:Waiting for result...
    INFO:kafkup.partition_reader.PartitionReaderProcess:New block finished: BlockMetadata(block_number=1, start={TopicPartition(topic='test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28', partition=2): 99}, end={TopicPartition(topic='test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28', partition=2): 199}, counters={TopicPartition(topic='test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28', partition=2): 100})
    INFO:root:Waiting for result...
    INFO:root:Waiting for result...
    INFO:kafkup.partition_reader.PartitionReaderProcess:New block finished: BlockMetadata(block_number=2, start={TopicPartition(topic='test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28', partition=2): 199}, end={TopicPartition(topic='test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28', partition=2): 299}, counters={TopicPartition(topic='test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28', partition=2): 100})
    INFO:root:Waiting for result...
    INFO:root:Waiting for result...
    INFO:kafkup.partition_reader.PartitionReaderProcess:New block finished: BlockMetadata(block_number=3, start={TopicPartition(topic='test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28', partition=2): 299}, end={TopicPartition(topic='test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28', partition=2): 399}, counters={TopicPartition(topic='test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28', partition=2): 100})

You can change some configuration parameters like:

* `export KAFKUP_MAX_MESSAGES_PER_BLOCK=1000` to configure how many messages should contain each block
* `export KAFKUP_PRETTY_PRINT_LEVEL=1` to dump objects in a prettier format

With `KAFKUP_PRETTY_PRINT_LEVEL=1`, output is:

    Using TOPIC_PARTITIONS=test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28:0,test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28:1,test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28:2
    INFO:root:Waiting for result...
    INFO:kafkup.block_storage.S3Client:get_all_keys_for_backup(): getting list of files for from-cli-1689929190
    INFO:kafkup.block_storage.S3Client:get_all_keys_for_backup(): Done. Got 0 files
    INFO:kafkup.partition_reader.PartitionReaderProcess:There is no last_block_metadata
    INFO:kafkup.kafka_client.KafkaClient:build_assign_partitions_list() topic=test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28 partition=0 offset=-2
    INFO:kafkup.kafka_client.KafkaClient:build_assign_partitions_list() topic=test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28 partition=1 offset=-2
    INFO:kafkup.kafka_client.KafkaClient:build_assign_partitions_list() topic=test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28 partition=2 offset=-2
    INFO:kafkup.partition_reader.PartitionReaderProcess:Will assign consumer to offsets: [PartitionOffset(topic='test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28', partition=0, offset=-2), PartitionOffset(topic='test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28', partition=1, offset=-2), PartitionOffset(topic='test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28', partition=2, offset=-2)]
    INFO:kafkup.partition_reader.PartitionReaderProcess:New block finished:
    +----------------------+-------+
    | Name                 | Value |
    +----------------------+-------+
    | Block number         |     0 |
    | # messages in block  |   100 |
    | # partitions @ start |     0 |
    | # partitions @ end   |     1 |
    +----------------------+-------+
    +-------------------------------------------+-----------+-------+-----+-------+
    | Topic                                     | Partition | Start | End | Count |
    +-------------------------------------------+-----------+-------+-----+-------+
    | test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28 |         2 |       |  99 |   100 |
    +-------------------------------------------+-----------+-------+-----+-------+
    INFO:root:Waiting for result...
    INFO:root:Waiting for result...
    INFO:kafkup.partition_reader.PartitionReaderProcess:New block finished:
    +----------------------+-------+
    | Name                 | Value |
    +----------------------+-------+
    | Block number         |     1 |
    | # messages in block  |   100 |
    | # partitions @ start |     1 |
    | # partitions @ end   |     1 |
    +----------------------+-------+
    +-------------------------------------------+-----------+-------+-----+-------+
    | Topic                                     | Partition | Start | End | Count |
    +-------------------------------------------+-----------+-------+-----+-------+
    | test-b2d12c5f-fa3f-4852-9cc0-79e1cf62cc28 |         2 |    99 | 199 |   100 |
    +-------------------------------------------+-----------+-------+-----+-------+
    INFO:root:Waiting for result...
    INFO:root:Waiting for result...


### Validate a backup

To validate a backup:

    env \
        KAFKUP_CONFIGURATION_FILE=config.json \
        KAFKUP_BACKUP_NAME=from-cli-1689929190 \
            python3 -m kafkup.cli.block_storage_validate_backup
