from uuid import uuid4

import pytest

from kafkup.configuration import Configuration


def test_with_overrides(configuration: Configuration):
    original_backup_name = configuration.backup_name
    assert original_backup_name

    new_backup_name = str(uuid4())
    new_configuration = configuration.with_overrides(backup_name=new_backup_name)

    assert new_configuration.backup_name == new_backup_name
    assert new_configuration.backup_name != configuration.backup_name


def test_override_fails_with_invalid_keys(configuration: Configuration):
    with pytest.raises(KeyError):
        configuration.with_overrides(this_is_not_a_valid_config_key="it's not")
