"""
Run these benchmarks with `pytest -vs` so you can see the results correctly:

    $ pytest -vs tests/benchmarks.py::TestCompareInstantiation
    tests/benchmarks.py::TestCompareInstantiation::test_instantiate_event_using_from_consumed
    > result=0.849		11.79K msg/sec				 PASSED
    tests/benchmarks.py::TestCompareInstantiation::test_instantiate_namedtuple
    > result=0.358		27.94K msg/sec				 PASSED
    tests/benchmarks.py::TestCompareInstantiation::test_instantiate_slot
    > result=0.215		46.42K msg/sec				 PASSED

You can run everything, a class or a specific method:

    $ pytest -vs tests/benchmarks.py
    $ pytest -vs tests/benchmarks.py::TestCompareInstantiation
    $ pytest -vs tests/benchmarks.py::TestCompareInstantiation::test_instantiate_slot
"""
from __future__ import annotations

import dataclasses
import io
import os
import pickle
import timeit
from collections import defaultdict
from collections import namedtuple

import pytest
from confluent_kafka import Message as KafkaMessage

from kafkup.block import TopicPartition
from kafkup.block import as_tuple
from tests.kafka_mock import Scenario

_MessageTuple = namedtuple("_MessageTuple", ["topic", "partition", "offset", "key", "value", "timestamp", "headers"])


def _build_namedtuple(kafka_message: KafkaMessage) -> _MessageTuple:
    headers = kafka_message.headers() or {}
    return _MessageTuple(
        topic=kafka_message.topic(),
        partition=kafka_message.partition(),
        offset=kafka_message.offset(),
        key=kafka_message.key(),
        value=kafka_message.value(),
        timestamp=kafka_message.timestamp(),
        headers=[(k, v) for k, v in headers],
    )


@dataclasses.dataclass(frozen=True)
class _Header:
    key: str
    value: bytes


@dataclasses.dataclass(frozen=True)
class _Event:  # pylint: disable=too-many-instance-attributes
    topic: str
    partition: int
    offset: int
    timestamp: int
    timestamp_type: int
    key: bytes
    value: bytes
    headers: list[_Header]

    @classmethod
    def from_consumed(cls, kafka_message: KafkaMessage) -> _Event:
        timestamp_type, timestamp = kafka_message.timestamp()
        kafka_headers = kafka_message.headers() or []
        return _Event(
            topic=kafka_message.topic(),
            partition=kafka_message.partition(),
            offset=kafka_message.offset(),
            timestamp=timestamp,
            timestamp_type=timestamp_type,
            key=kafka_message.key(),
            value=kafka_message.value(),
            headers=[_Header(key=key, value=value) for key, value in kafka_headers],
        )


class _EventSlot:  # pylint: disable=too-many-instance-attributes
    __slots__ = ("topic", "partition", "offset", "timestamp", "timestamp_type", "key", "value", "headers")

    def __init__(self, kafka_message: KafkaMessage):
        timestamp_type, timestamp = kafka_message.timestamp()
        kafka_headers = kafka_message.headers() or []
        self.topic = kafka_message.topic()
        self.partition = kafka_message.partition()
        self.offset = kafka_message.offset()
        self.timestamp = timestamp
        self.timestamp_type = timestamp_type
        self.key = kafka_message.key()
        self.value = kafka_message.value()
        self.headers = [(key, value) for key, value in kafka_headers]


@dataclasses.dataclass()
class Config:
    num_messages: int = 10_000
    timeit_number: int = 50
    timeit_repeat: int = 5

    @classmethod
    def get(cls):
        if os.environ.get("BENCHMARK_QUICK", "0") == "1":
            return Config(num_messages=5_000, timeit_number=20, timeit_repeat=1)
        return Config()


@pytest.fixture(scope="session")
def config() -> Config:
    return Config.get()


@pytest.fixture(scope="session")
def scenario(config: Config) -> Scenario:
    scenario = Scenario.create(config.num_messages)
    return scenario


@pytest.fixture(scope="session")
def kafka_messages(scenario) -> list[KafkaMessage]:
    return [Scenario.expected_when_consumed(_, topic="dummy-topic") for _ in scenario.all_events]


@pytest.fixture(scope="session")
def events(kafka_messages) -> list[_Event]:
    return [_Event.from_consumed(_) for _ in kafka_messages]


@pytest.fixture(scope="session")
def message_tuples(kafka_messages) -> list[_MessageTuple]:
    return [_build_namedtuple(_) for _ in kafka_messages]


@pytest.fixture(scope="session")
def event_slots(kafka_messages) -> list[_EventSlot]:
    return [_EventSlot(_) for _ in kafka_messages]


def _print(config, result):
    print(f"\n> result={result:.3f}\t\t{(config.num_messages / 1000.0) / result:.2f}K msg/sec\t\t\t\t", end=" ")


class TestCompareInstantiation:
    def test_instantiate_event_using_from_consumed(self, config, kafka_messages):
        assert _Event.from_consumed(kafka_messages[0]).value == kafka_messages[0].value()

        def run():
            for _ in kafka_messages:
                _Event.from_consumed(_)

        bench = timeit.Timer(stmt="run()", globals=locals())
        result = min(bench.repeat(repeat=config.timeit_repeat, number=config.timeit_number))
        _print(config, result)

    def test_instantiate_namedtuple(self, config, kafka_messages):
        assert _build_namedtuple(kafka_messages[0]).value == kafka_messages[0].value()

        def run():
            for _ in kafka_messages:
                _build_namedtuple(_)

        bench = timeit.Timer(stmt="run()", globals=locals())
        result = min(bench.repeat(repeat=config.timeit_repeat, number=config.timeit_number))
        _print(config, result)

    def test_instantiate_slot(self, config, kafka_messages):
        assert _EventSlot(kafka_messages[0]).value == kafka_messages[0].value()

        def run():
            for _ in kafka_messages:
                _EventSlot(_)

        bench = timeit.Timer(stmt="run()", globals=locals())
        result = min(bench.repeat(repeat=config.timeit_repeat, number=config.timeit_number))
        _print(config, result)

    def test_instantiate_tuple(self, config, kafka_messages):
        assert any(
            (
                kafka_messages[0].topic(),
                kafka_messages[0].partition(),
                kafka_messages[0].offset(),
                kafka_messages[0].timestamp(),
                kafka_messages[0].key(),
                kafka_messages[0].value(),
                kafka_messages[0].headers(),
            )
        )

        def run():
            for _ in kafka_messages:
                (
                    _.topic(),
                    _.partition(),
                    _.offset(),
                    _.timestamp(),
                    _.key(),
                    _.value(),
                    _.headers(),
                )

        bench = timeit.Timer(stmt="run()", globals=locals())
        result = min(bench.repeat(repeat=config.timeit_repeat, number=config.timeit_number))
        _print(config, result)

    def test_instantiate_as_tuple(self, config, kafka_messages):
        assert any(as_tuple(kafka_messages[0]))

        def run():
            for _ in kafka_messages:
                as_tuple(_)

        bench = timeit.Timer(stmt="run()", globals=locals())
        result = min(bench.repeat(repeat=config.timeit_repeat, number=config.timeit_number))
        _print(config, result)


class TestComparePickleList:
    def test_pickle_events(self, config, events):
        assert pickle.dumps(events[0])

        def run():
            pickle.dumps(events, protocol=pickle.HIGHEST_PROTOCOL)

        bench = timeit.Timer(stmt="run()", globals=locals())
        result = min(bench.repeat(repeat=config.timeit_repeat, number=config.timeit_number))
        _print(config, result)

    def test_pickle_message_tuples(self, config, message_tuples):
        assert pickle.dumps(message_tuples[0])

        def run():
            pickle.dumps(message_tuples, protocol=pickle.HIGHEST_PROTOCOL)

        bench = timeit.Timer(stmt="run()", globals=locals())
        result = min(bench.repeat(repeat=config.timeit_repeat, number=config.timeit_number))
        _print(config, result)

    def test_pickle_event_slots(self, config, event_slots):
        assert pickle.dumps(event_slots[0])

        def run():
            pickle.dumps(event_slots, protocol=pickle.HIGHEST_PROTOCOL)

        bench = timeit.Timer(stmt="run()", globals=locals())
        result = min(bench.repeat(repeat=config.timeit_repeat, number=config.timeit_number))
        _print(config, result)

    def test_create_and_pickle_events(self, config, kafka_messages):
        def run():
            pickle.dumps([_Event.from_consumed(_) for _ in kafka_messages], protocol=pickle.HIGHEST_PROTOCOL)

        bench = timeit.Timer(stmt="run()", globals=locals())
        result = min(bench.repeat(repeat=config.timeit_repeat, number=config.timeit_number))
        _print(config, result)

    def test_create_and_pickle_message_tuples(self, config, kafka_messages):
        def run():
            pickle.dumps([_build_namedtuple(_) for _ in kafka_messages], protocol=pickle.HIGHEST_PROTOCOL)

        bench = timeit.Timer(stmt="run()", globals=locals())
        result = min(bench.repeat(repeat=config.timeit_repeat, number=config.timeit_number))
        _print(config, result)

    def test_create_and_pickle_event_slots(self, config, kafka_messages):
        def run():
            pickle.dumps([_EventSlot(_) for _ in kafka_messages], protocol=pickle.HIGHEST_PROTOCOL)

        bench = timeit.Timer(stmt="run()", globals=locals())
        result = min(bench.repeat(repeat=config.timeit_repeat, number=config.timeit_number))
        _print(config, result)


class TestComparePickleInstances:
    def test_pickle_event_to_byteio(self, config, events):
        def run():
            output_buffer = io.BytesIO()
            for _ in events:
                pickle.dump(_, output_buffer, protocol=pickle.HIGHEST_PROTOCOL)

        bench = timeit.Timer(stmt="run()", globals=locals())
        result = min(bench.repeat(repeat=config.timeit_repeat, number=config.timeit_number))
        _print(config, result)

    def test_pickle_message_tuples_to_byteio(self, config, message_tuples):
        def run():
            output_buffer = io.BytesIO()
            for _ in message_tuples:
                pickle.dump(_, output_buffer, protocol=pickle.HIGHEST_PROTOCOL)

        bench = timeit.Timer(stmt="run()", globals=locals())
        result = min(bench.repeat(repeat=config.timeit_repeat, number=config.timeit_number))
        _print(config, result)

    def test_pickle_payload_items_to_byteio(self, config, kafka_messages):
        def run():
            output_buffer = io.BytesIO()
            for message in kafka_messages:
                pickle.dump(message.topic(), output_buffer, protocol=pickle.HIGHEST_PROTOCOL)
                pickle.dump(message.partition(), output_buffer, protocol=pickle.HIGHEST_PROTOCOL)
                pickle.dump(message.offset(), output_buffer, protocol=pickle.HIGHEST_PROTOCOL)
                pickle.dump(message.key(), output_buffer, protocol=pickle.HIGHEST_PROTOCOL)
                pickle.dump(message.value(), output_buffer, protocol=pickle.HIGHEST_PROTOCOL)
                pickle.dump(message.timestamp(), output_buffer, protocol=pickle.HIGHEST_PROTOCOL)

                headers = message.headers() or []
                pickle.dump(headers, output_buffer, protocol=pickle.HIGHEST_PROTOCOL)

        bench = timeit.Timer(stmt="run()", globals=locals())
        result = min(bench.repeat(repeat=config.timeit_repeat, number=config.timeit_number))
        _print(config, result)

    def test_pickle_payload_tuple_to_byteio(self, config, kafka_messages):
        def run():
            output_buffer = io.BytesIO()
            for message in kafka_messages:
                pickle.dump(
                    (
                        message.topic(),
                        message.partition(),
                        message.offset(),
                        message.timestamp(),
                        message.key(),
                        message.value(),
                        message.headers(),
                    ),
                    output_buffer,
                    protocol=pickle.HIGHEST_PROTOCOL,
                )

        bench = timeit.Timer(stmt="run()", globals=locals())
        result = min(bench.repeat(repeat=config.timeit_repeat, number=config.timeit_number))
        _print(config, result)


class TestComparePickleItemsVsList:
    def test_pickle_message_tuples_items(self, config, message_tuples):
        def run():
            output_buffer = io.BytesIO()
            pickle.dump(message_tuples, output_buffer, protocol=pickle.HIGHEST_PROTOCOL)

        bench = timeit.Timer(stmt="run()", globals=locals())
        result = min(bench.repeat(repeat=config.timeit_repeat, number=config.timeit_number))
        _print(config, result)

    def test_pickle_message_tuples_list(self, config, message_tuples):
        def run():
            output_buffer = io.BytesIO()
            for _ in message_tuples:
                pickle.dump(_, output_buffer, protocol=pickle.HIGHEST_PROTOCOL)

        bench = timeit.Timer(stmt="run()", globals=locals())
        result = min(bench.repeat(repeat=config.timeit_repeat, number=config.timeit_number))
        _print(config, result)


class TestIncrementalBlockMetadata:
    @dataclasses.dataclass
    class _IncrementalBlockMetadata:
        block_number: int
        start: dict[TopicPartition, int]
        end: dict[TopicPartition, int]
        counters: dict[TopicPartition, int]

        @classmethod
        def new_root_block(cls):
            return cls(start={}, end={}, counters={}, block_number=0)

        def update(self, message_tuple: _MessageTuple):
            topic_partition = TopicPartition(topic=message_tuple.topic, partition=message_tuple.partition)
            # update `end`
            if topic_partition in self.end:
                pass
            self.end[topic_partition] = message_tuple.offset

            # update `counters`
            if topic_partition not in self.counters:
                self.counters[topic_partition] = 0
            self.counters[topic_partition] += 1

    @dataclasses.dataclass
    class _TupleKeyedIncrementalBlockMetadata:
        block_number: int
        start: dict[tuple[str, int], int]
        end: dict[tuple[str, int], int]
        counters: dict[tuple[str, int], int]

        @classmethod
        def new_root_block(cls):
            return cls(start={}, end={}, counters={}, block_number=0)

        def update(self, message_tuple: _MessageTuple):
            topic_partition = (message_tuple.topic, message_tuple.partition)
            if topic_partition in self.end:
                pass
            self.end[topic_partition] = message_tuple.offset

            # update `counters`
            if topic_partition not in self.counters:
                self.counters[topic_partition] = 0
            self.counters[topic_partition] += 1

    @dataclasses.dataclass
    class _TupleKeyedIncrementalBlockMetadataDefaultDict:
        block_number: int
        start: dict[tuple[str, int], int]
        end: dict[tuple[str, int], int]
        counters: dict[tuple[str, int], int]

        @classmethod
        def new_root_block(cls):
            return cls(start={}, end={}, counters=defaultdict(lambda: 0), block_number=0)

        def update(self, message_tuple: _MessageTuple):
            topic_partition = (message_tuple.topic, message_tuple.partition)
            if topic_partition in self.end:
                pass
            self.end[topic_partition] = message_tuple.offset

            # update `counters`
            self.counters[topic_partition] += 1

    def test_topic_partition_keyed_dict(self, config, message_tuples):
        ibmd = self._IncrementalBlockMetadata.new_root_block()

        def run():
            for _ in message_tuples:
                ibmd.update(_)

        bench = timeit.Timer(stmt="run()", globals=locals())
        result = min(bench.repeat(repeat=config.timeit_repeat, number=config.timeit_number))
        _print(config, result)

    def test_tuple_keyed_dict(self, config, message_tuples):
        ibmd = self._TupleKeyedIncrementalBlockMetadata.new_root_block()

        def run():
            for _ in message_tuples:
                ibmd.update(_)

        bench = timeit.Timer(stmt="run()", globals=locals())
        result = min(bench.repeat(repeat=config.timeit_repeat, number=config.timeit_number))
        _print(config, result)

    def test_tuple_keyed_defaultdict(self, config, message_tuples):
        ibmd = self._TupleKeyedIncrementalBlockMetadataDefaultDict.new_root_block()

        def run():
            for _ in message_tuples:
                ibmd.update(_)

        bench = timeit.Timer(stmt="run()", globals=locals())
        result = min(bench.repeat(repeat=config.timeit_repeat, number=config.timeit_number))
        _print(config, result)
