from kafkup.configuration import Configuration
from kafkup.kafka_client import KafkaClient
from kafkup.kafka_client import TopicMetadata
from kafkup.kafka_client import TopicsMetadata
from tests.conftest import kafka_required
from tests.kafka_mock import generate_random_payload_to_produce


@kafka_required
def test_get_topics_metadata(create_topic, configuration: Configuration):
    topic = create_topic()
    client = KafkaClient(configuration=configuration)
    metadata: TopicsMetadata = client.get_topics_metadata()

    metadata_dict: dict[str, TopicMetadata] = {_.topic: _ for _ in metadata.topics}
    assert topic in metadata_dict

    topic_metadata = metadata_dict[topic]
    assert {(_.partition, _.offset) for _ in topic_metadata.partitions_metadata} == {(0, None), (1, None), (2, None)}


@kafka_required
def test_generate_random_payload_to_produce(topic, kafka_producer):
    def delivery_report(err, msg):
        if err:
            raise Exception(f"Message not delivered: {err}")

    for _ in range(10):
        kafka_producer.poll(0)
        payload = generate_random_payload_to_produce()
        kafka_producer.produce(topic=topic, callback=delivery_report, **payload)
        kafka_producer.flush()
