from types import NoneType

import pytest
from confluent_kafka import OFFSET_BEGINNING
from confluent_kafka import Producer

from kafkup.block import PartitionOffset
from kafkup.configuration import Configuration
from kafkup.kafka_client import KafkaClient
from tests.conftest import kafka_required
from tests.kafka_mock import Scenario


@pytest.mark.skip
def test_consume_error():
    ...


@kafka_required
def test_compare_mock_and_consumed(topic: str, populator, configuration: Configuration):
    scenario = Scenario.create(num_messages=100)
    populator(topic, scenario=scenario)
    client = KafkaClient(configuration=configuration)
    consumer = client.get_consumer_for_partitions(
        partitions=[
            PartitionOffset(
                topic=topic,
                partition=_,
                offset=OFFSET_BEGINNING,
            )
            for _ in [0, 1, 2]
        ]
    )
    consumed_messages = []
    for _ in range(len(scenario.all_events)):
        consumed_messages.append(consumer.poll())

    assert len(consumed_messages) == len(scenario.all_events)
    consumed_messages_p0 = [ce for ce in consumed_messages if ce.partition() == 0]
    assert len(consumed_messages_p0) == len(scenario.partition_0)

    null_header_found = False
    non_null_header_found = False

    # now let's compare the events received from Kafka, and the mocked via KafkaConsumedMessageMock
    for real_message, scenario_item in zip(consumed_messages_p0, scenario.partition_0):
        mocked = Scenario.expected_when_consumed(scenario_item, topic=topic)

        real_headers = real_message.headers()
        if real_headers is None:
            null_header_found = True
        else:
            assert len(real_headers) > 0, "Empty list of headers... It's assumed headers is None, or non-empty"
            non_null_header_found = True
            assert isinstance(real_headers, list)
            for a_header in real_headers:
                assert isinstance(a_header, tuple)
                assert isinstance(a_header[0], str)
                assert isinstance(a_header[1], bytes)

        assert real_message.topic() == mocked.topic()
        assert real_message.partition() == mocked.partition()
        assert real_message.offset() >= mocked.offset()
        assert real_message.key() == mocked.key()
        assert real_message.value() == mocked.value()
        assert real_message.headers() == mocked.headers()
        assert real_message.timestamp()[0] == mocked.timestamp()[0]
        assert (
            abs(real_message.timestamp()[1] - mocked.timestamp()[1]) < 120_000
        ), f"real={real_message.timestamp()} mocked={mocked.timestamp()}"

    assert null_header_found, "All messages had a header. Run again, sometimes happens :/"
    assert non_null_header_found, "All messages didn't have header. Run again, just in case"


def _delivery_report(err, msg):
    if err is not None:
        raise Exception(f"Message not delivered: {err}")


@kafka_required
def test_check_types(topic: str, kafka_producer: Producer, configuration: Configuration):
    kafka_producer.produce(topic=topic, partition=0, key=None, value=b"value-0", on_delivery=_delivery_report)
    kafka_producer.produce(topic=topic, partition=0, key=b"key-1", value=b"value-1", on_delivery=_delivery_report)
    kafka_producer.produce(
        topic=topic, partition=0, value=b"value-2", headers=[("h1", b"v1")], on_delivery=_delivery_report
    )
    kafka_producer.flush()

    client = KafkaClient(configuration=configuration)
    consumer = client.get_consumer_for_partitions(
        partitions=[
            PartitionOffset(
                topic=topic,
                partition=0,
                offset=OFFSET_BEGINNING,
            )
        ]
    )
    consumed_messages = [consumer.poll(), consumer.poll(), consumer.poll()]

    for _ in consumed_messages:
        assert isinstance(_.topic(), str)
        assert isinstance(_.partition(), int)
        assert isinstance(_.offset(), int)
        assert isinstance(_.timestamp(), tuple)
        assert isinstance(_.timestamp()[0], int)
        assert isinstance(_.timestamp()[1], int)
        assert isinstance(_.headers(), (list, NoneType))
        assert isinstance(_.key(), (bytes, NoneType))
        assert isinstance(_.value(), bytes)

    assert isinstance(consumed_messages[0].key(), NoneType)
    assert isinstance(consumed_messages[0].headers(), NoneType)

    assert isinstance(consumed_messages[1].key(), bytes)
    assert isinstance(consumed_messages[1].headers(), NoneType)

    assert isinstance(consumed_messages[2].key(), NoneType)
    assert isinstance(consumed_messages[2].headers(), list)
    assert isinstance(consumed_messages[2].headers()[0], tuple)
    assert isinstance(consumed_messages[2].headers()[0][0], str)
    assert isinstance(consumed_messages[2].headers()[0][1], bytes)
