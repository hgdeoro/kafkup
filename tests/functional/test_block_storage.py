from collections.abc import Callable
from uuid import uuid4

import pytest

from kafkup.block import Block
from kafkup.block_builder import InMemoryBlockBuilder
from kafkup.block_storage import BackupNotInitializedError
from kafkup.block_storage import S3BlockStorage
from kafkup.block_storage import S3Client
from kafkup.configuration import Configuration
from tests.conftest import minio_required


def _clean_parsed(original: dict, ignore) -> dict:
    return {k: v for k, v in original.items() if k not in ignore}


@pytest.mark.skip
def test_get_all_blocks_metadata():
    ...


@minio_required
def test_upload_works(random_block, s3_bucket, configuration: Configuration):
    storage = S3BlockStorage(configuration=configuration)
    block: Block = random_block()

    all_s3_keys_before_upload = [_.key for _ in s3_bucket.objects.filter(Prefix=f"{configuration.backup_name}/")]
    result = storage.serialize_and_upload(block)
    all_s3_keys_after_upload = [_.key for _ in s3_bucket.objects.filter(Prefix=f"{configuration.backup_name}/")]

    # data
    parsed_key = S3Client.parse_key(result.data_path)
    assert parsed_key.key not in all_s3_keys_before_upload
    assert parsed_key.key in all_s3_keys_after_upload

    # metadata
    parsed_key = S3Client.parse_key(result.metadata_path)
    assert parsed_key.key not in all_s3_keys_before_upload
    assert parsed_key.key in all_s3_keys_after_upload


@minio_required
def test_retrieve_data_and_metadata(random_block, s3_bucket, configuration: Configuration):
    storage = S3BlockStorage(configuration=configuration)
    block: Block = random_block()
    result = storage.serialize_and_upload(block)

    metadata_key = S3Client.parse_key(result.metadata_path).key
    metadata = storage.download_metadata(metadata_key)
    data = storage.download_data(metadata)

    assert data == block.data
    assert metadata == block.metadata


def test_parse_key_invalid():
    parsed = S3Client.parse_key("random")
    assert parsed is not None
    assert parsed.key == "random"
    assert parsed.parsed is False
    assert parsed.full_path is None
    assert parsed.backup_name is None
    assert parsed.filename is None
    assert parsed.data_metadata is None
    assert parsed.block_number is None
    assert parsed.jitter is None
    assert parsed.file_ext is None


@pytest.mark.parametrize("key", ["s3://bu/ba/meta-123-jitter.exe", "ba/meta-123-jitter.exe"])
def test_parse_path_valid(key):
    parsed = S3Client.parse_key(key)
    assert parsed is not None
    assert parsed.parsed is True
    assert parsed.full_path == "ba/meta-123-jitter.exe"
    assert parsed.backup_name == "ba"
    assert parsed.filename == "meta-123-jitter.exe"
    assert parsed.data_metadata == "meta"
    assert parsed.block_number == "123"
    assert parsed.jitter == "jitter"
    assert parsed.file_ext == "exe"
    assert parsed.full_path == parsed.key


@pytest.mark.parametrize(
    "key",
    [
        "s3://kafkup/b1c9fec0-b1d4-4295-8b74-5f8d2f73d9cd/block-0-c352d352b7584a10a514aee990f23777.pickle",
        "b1c9fec0-b1d4-4295-8b74-5f8d2f73d9cd/block-0-c352d352b7584a10a514aee990f23777.pickle",
    ],
)
def test_parse_path_real(key):
    parsed = S3Client.parse_key(key)
    assert parsed is not None
    assert parsed.parsed is True
    assert parsed.full_path == "b1c9fec0-b1d4-4295-8b74-5f8d2f73d9cd/block-0-c352d352b7584a10a514aee990f23777.pickle"
    assert parsed.backup_name == "b1c9fec0-b1d4-4295-8b74-5f8d2f73d9cd"
    assert parsed.filename == "block-0-c352d352b7584a10a514aee990f23777.pickle"
    assert parsed.data_metadata == "block"
    assert parsed.block_number == "0"
    assert parsed.jitter == "c352d352b7584a10a514aee990f23777"
    assert parsed.file_ext == "pickle"
    assert parsed.full_path == parsed.key


@minio_required
def test_get_last_block_metadata_when_only_single_block_exists(random_block, s3_bucket, configuration: Configuration):
    storage = S3BlockStorage(configuration=configuration)
    block = random_block()
    storage.serialize_and_upload(block)

    latest_metadata = storage.get_latest_block_metadata()
    assert latest_metadata

    assert latest_metadata.block_number == block.metadata.block_number
    assert latest_metadata == block.metadata


@minio_required
def test_get_last_block_metadata_return_latest(random_block, s3_bucket, configuration: Configuration):
    storage = S3BlockStorage(configuration=configuration)
    blocks = [random_block() for _ in range(3)]
    for block in blocks:
        storage.serialize_and_upload(block)

    latest_metadata = storage.get_latest_block_metadata()
    assert latest_metadata

    assert latest_metadata.block_number == blocks[-1].metadata.block_number


@minio_required
def test_get_last_block_metadata_for_non_initialized_backup(configuration: Configuration):
    configuration = configuration.with_overrides(backup_name=str(uuid4()))
    storage = S3BlockStorage(configuration=configuration)

    with pytest.raises(BackupNotInitializedError):
        storage.get_latest_block_metadata()


@minio_required
def test_validation_works_for_simple_backup(configuration: Configuration, block_chain_generate: Callable):
    configuration = configuration.with_overrides(max_messages_per_block=10)
    block_builder = InMemoryBlockBuilder(configuration=configuration)
    blocks = block_chain_generate(configuration, scenario_size=50, block_builder=block_builder)

    storage = S3BlockStorage(configuration=configuration)
    for block in blocks:
        storage.serialize_and_upload(block)

    assert len(storage.get_all_blocks_metadata()) == len(blocks)

    storage.validate()


@minio_required
def test_get_last_block_metadata_for_initialized_backup(s3_bucket, configuration: Configuration, block_chain_generate):
    configuration = configuration.with_overrides(max_messages_per_block=10)
    storage = S3BlockStorage(configuration=configuration)
    block_builder = InMemoryBlockBuilder(configuration=configuration)

    blocks = block_chain_generate(configuration, scenario_size=50, block_builder=block_builder)[0:3]
    for block in blocks:
        storage.serialize_and_upload(block)

    latest_metadata = storage.get_latest_block_metadata()
    assert latest_metadata

    assert latest_metadata.block_number == blocks[-1].metadata.block_number
    assert latest_metadata == blocks[-1].metadata
