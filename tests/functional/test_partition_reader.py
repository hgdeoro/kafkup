import logging

from confluent_kafka import Consumer
from confluent_kafka import Producer

from kafkup import partition_reader
from kafkup.block import BlockMetadata
from kafkup.block import TopicPartition
from kafkup.block_builder import InMemoryBlockBuilder
from kafkup.block_storage import S3BlockStorage
from kafkup.configuration import Configuration
from tests.conftest import kafka_required
from tests.kafka_mock import Scenario

logger = logging.getLogger(__name__)


@kafka_required
def test_functional(
    topic: str,
    populator,
    kafka_consumer: Consumer,
    kafka_producer: Producer,
    create_topic,
    kafkacat_dump,
    configuration: Configuration,
):
    # setup topics
    scenario = Scenario.create()
    configuration = configuration.with_overrides(max_messages_per_block=len(scenario.all_events))
    partitions = populator(topic, scenario)

    # launch process
    consumer = partition_reader.PartitionReaderBase.as_thread()(
        daemon=True,
        configuration=configuration,
        partitions=partitions,
        block_builder=InMemoryBlockBuilder(configuration=configuration),
        block_storage=S3BlockStorage(configuration=configuration),
    )
    consumer.start()

    # let's get result from child process
    result = consumer.result_queue.get(timeout=3)
    assert result["status"] == partition_reader.STATUS_NEW_BLOCK_FINISHED, f"result: {result}"
    metadata: BlockMetadata = result["metadata"]
    assert metadata.counters[TopicPartition(topic, 0)] == len(scenario.partition_0)
    assert metadata.counters[TopicPartition(topic, 1)] == len(scenario.partition_1)
    assert metadata.counters[TopicPartition(topic, 2)] == len(scenario.partition_2)

    result = consumer.result_queue.get(timeout=3)
    assert result["status"] == partition_reader.STATUS_BLOCK_UPLOADED, f"result: {result}"
