from kafkup.block import KAFKA_MESSAGE_TUPLE_KEY
from kafkup.block import KAFKA_MESSAGE_TUPLE_OFFSET
from kafkup.block import KAFKA_MESSAGE_TUPLE_PARTITION
from kafkup.block import KAFKA_MESSAGE_TUPLE_TOPIC
from kafkup.block import KAFKA_MESSAGE_TUPLE_VALUE
from kafkup.block import BlockMetadata
from kafkup.block import TopicPartition
from kafkup.block_builder import InMemoryBlockBuilder
from kafkup.configuration import Configuration
from tests.conftest import KafkaConsumedMessageMock


def test_in_memory_block_builder_block_data(configuration: Configuration):
    configuration = configuration.with_overrides(max_messages_per_block=2)
    builder = InMemoryBlockBuilder(configuration=configuration)
    kafka_event_1 = KafkaConsumedMessageMock.random()
    kafka_event_2 = KafkaConsumedMessageMock.random()
    events = [kafka_event_1, kafka_event_2]

    assert builder.handle_event(kafka_event_1) is None
    block = builder.handle_event(kafka_event_2)
    assert block is not None

    assert len(block.data.events) == 2
    assert {_[KAFKA_MESSAGE_TUPLE_KEY] for _ in block.data.events} == {_.key() for _ in events}
    assert {_[KAFKA_MESSAGE_TUPLE_VALUE] for _ in block.data.events} == {_.value() for _ in events}
    assert {
        (_[KAFKA_MESSAGE_TUPLE_TOPIC], _[KAFKA_MESSAGE_TUPLE_PARTITION], _[KAFKA_MESSAGE_TUPLE_OFFSET])
        for _ in block.data.events
    } == {(_.topic(), _.partition(), _.offset()) for _ in events}


def test_in_memory_block_builder_block_metadata(configuration: Configuration):
    configuration = configuration.with_overrides(max_messages_per_block=10)
    builder = InMemoryBlockBuilder(configuration=configuration)

    assert builder.handle_event(KafkaConsumedMessageMock.random(topic="topic-a", partition=0, offset=234)) is None
    assert builder.handle_event(KafkaConsumedMessageMock.random(topic="topic-a", partition=0, offset=235)) is None
    assert builder.handle_event(KafkaConsumedMessageMock.random(topic="topic-a", partition=0, offset=236)) is None
    assert builder.handle_event(KafkaConsumedMessageMock.random(topic="topic-a", partition=1, offset=555)) is None
    assert builder.handle_event(KafkaConsumedMessageMock.random(topic="topic-b", partition=0, offset=876)) is None
    assert builder.handle_event(KafkaConsumedMessageMock.random(topic="topic-b", partition=0, offset=877)) is None
    assert builder.handle_event(KafkaConsumedMessageMock.random(topic="topic-b", partition=1, offset=222)) is None
    assert builder.handle_event(KafkaConsumedMessageMock.random(topic="topic-b", partition=1, offset=223)) is None
    assert builder.handle_event(KafkaConsumedMessageMock.random(topic="topic-c", partition=3, offset=9)) is None
    block = builder.handle_event(KafkaConsumedMessageMock.random(topic="topic-d", partition=6, offset=4123))

    assert block is not None

    assert block.metadata.block_number == 0
    assert block.metadata.start == {}
    assert block.metadata.end == {
        TopicPartition("topic-a", 0): 236,
        TopicPartition("topic-a", 1): 555,
        TopicPartition("topic-b", 0): 877,
        TopicPartition("topic-b", 1): 223,
        TopicPartition("topic-c", 3): 9,
        TopicPartition("topic-d", 6): 4123,
    }
    assert block.metadata.counters == {
        TopicPartition("topic-a", 0): 3,
        TopicPartition("topic-a", 1): 1,
        TopicPartition("topic-b", 0): 2,
        TopicPartition("topic-b", 1): 2,
        TopicPartition("topic-c", 3): 1,
        TopicPartition("topic-d", 6): 1,
    }
    assert block.metadata.as_dict() == {
        "block_number": 0,
        "start": [],
        "end": [
            ["topic-a", 0, 236],
            ["topic-a", 1, 555],
            ["topic-b", 0, 877],
            ["topic-b", 1, 223],
            ["topic-c", 3, 9],
            ["topic-d", 6, 4123],
        ],
        "counters": [
            ["topic-a", 0, 3],
            ["topic-a", 1, 1],
            ["topic-b", 0, 2],
            ["topic-b", 1, 2],
            ["topic-c", 3, 1],
            ["topic-d", 6, 1],
        ],
    }
    assert BlockMetadata.from_dict(block.metadata.as_dict()) == block.metadata


def test_in_memory_block_builder_block_metadata_multiple_blocks(configuration: Configuration):
    configuration = configuration.with_overrides(max_messages_per_block=3)
    builder = InMemoryBlockBuilder(configuration=configuration)

    assert builder.handle_event(KafkaConsumedMessageMock.random(topic="topic-a", partition=0, offset=234)) is None
    assert builder.handle_event(KafkaConsumedMessageMock.random(topic="topic-a", partition=0, offset=235)) is None
    block_1 = builder.handle_event(KafkaConsumedMessageMock.random(topic="topic-a", partition=0, offset=236))

    assert builder.handle_event(KafkaConsumedMessageMock.random(topic="topic-a", partition=1, offset=555)) is None
    assert builder.handle_event(KafkaConsumedMessageMock.random(topic="topic-b", partition=0, offset=876)) is None
    block_2 = builder.handle_event(KafkaConsumedMessageMock.random(topic="topic-b", partition=1, offset=222))

    assert builder.handle_event(KafkaConsumedMessageMock.random(topic="topic-b", partition=1, offset=223)) is None
    assert builder.handle_event(KafkaConsumedMessageMock.random(topic="topic-b", partition=1, offset=224)) is None
    block_3 = builder.handle_event(KafkaConsumedMessageMock.random(topic="topic-c", partition=3, offset=9))

    assert builder.handle_event(KafkaConsumedMessageMock.random(topic="topic-d", partition=6, offset=4123)) is None

    assert block_1 is not None
    assert block_2 is not None
    assert block_3 is not None

    assert block_1.metadata.block_number == 0
    assert block_1.metadata.start == {}
    assert block_1.metadata.end == {
        TopicPartition("topic-a", 0): 236,
    }
    assert block_1.metadata.counters == {
        TopicPartition("topic-a", 0): 3,
    }
    assert block_1.metadata.as_dict() == {
        "block_number": 0,
        "start": [],
        "end": [["topic-a", 0, 236]],
        "counters": [["topic-a", 0, 3]],
    }
    assert BlockMetadata.from_dict(block_1.metadata.as_dict()) == block_1.metadata

    assert block_2.metadata.block_number == 1
    assert block_2.metadata.start == {
        TopicPartition("topic-a", 0): 236,
    }
    assert block_2.metadata.end == {
        TopicPartition("topic-a", 0): 236,
        TopicPartition("topic-a", 1): 555,
        TopicPartition("topic-b", 0): 876,
        TopicPartition("topic-b", 1): 222,
    }
    assert block_2.metadata.counters == {
        TopicPartition("topic-a", 1): 1,
        TopicPartition("topic-b", 0): 1,
        TopicPartition("topic-b", 1): 1,
    }
    assert block_2.metadata.as_dict() == {
        "block_number": 1,
        "start": [["topic-a", 0, 236]],
        "end": [["topic-a", 0, 236], ["topic-a", 1, 555], ["topic-b", 0, 876], ["topic-b", 1, 222]],
        "counters": [["topic-a", 1, 1], ["topic-b", 0, 1], ["topic-b", 1, 1]],
    }
    assert BlockMetadata.from_dict(block_2.metadata.as_dict()) == block_2.metadata

    assert block_3.metadata.block_number == 2
    assert block_3.metadata.start == {
        TopicPartition("topic-a", 0): 236,
        TopicPartition("topic-a", 1): 555,
        TopicPartition("topic-b", 0): 876,
        TopicPartition("topic-b", 1): 222,
    }
    assert block_3.metadata.end == {
        TopicPartition("topic-a", 0): 236,
        TopicPartition("topic-a", 1): 555,
        TopicPartition("topic-b", 0): 876,
        TopicPartition("topic-b", 1): 224,
        TopicPartition("topic-c", 3): 9,
    }
    assert block_3.metadata.counters == {
        TopicPartition("topic-b", 1): 2,
        TopicPartition("topic-c", 3): 1,
    }
    assert block_3.metadata.as_dict() == {
        "block_number": 2,
        "start": [["topic-a", 0, 236], ["topic-a", 1, 555], ["topic-b", 0, 876], ["topic-b", 1, 222]],
        "end": [["topic-a", 0, 236], ["topic-a", 1, 555], ["topic-b", 0, 876], ["topic-b", 1, 224], ["topic-c", 3, 9]],
        "counters": [["topic-b", 1, 2], ["topic-c", 3, 1]],
    }
    assert BlockMetadata.from_dict(block_3.metadata.as_dict()) == block_3.metadata
