from __future__ import annotations

import dataclasses
import json
import os
import random
import time
from collections import namedtuple
from random import randint
from uuid import uuid4

from confluent_kafka import TIMESTAMP_CREATE_TIME


@dataclasses.dataclass
class KafkaConsumedMessageMock:
    mock_topic: str
    mock_partition: int
    mock_offset: int
    mock_timestamp: tuple[int, int]
    mock_key: bytes | None
    mock_value: bytes | None
    mock_headers: list[tuple[str, bytes]] | None

    def error(self):
        return None

    def headers(self) -> list[tuple[str, bytes]] | None:
        return self.mock_headers

    def key(self):
        return self.mock_key

    def latency(self):
        raise NotImplementedError()

    def leader_epoch(self):
        raise NotImplementedError()

    def offset(self):
        return self.mock_offset

    def partition(self):
        return self.mock_partition

    def set_headers(self):
        raise NotImplementedError()

    def set_key(self):
        raise NotImplementedError()

    def set_value(self):
        raise NotImplementedError()

    def timestamp(self):
        return self.mock_timestamp

    def topic(self):
        return self.mock_topic

    def value(self, payload=None):  # FIXME: why `payload`?
        return self.mock_value

    # def __getattribute__(self):
    #     raise NotImplementedError()

    # def __init__(self):
    #     raise NotImplementedError()

    # def __len__(self):
    #     raise NotImplementedError()

    @classmethod
    def random(cls, topic=None, partition=None, offset=None):
        scenario_item = Scenario.create_scenario_item(
            partition=partition if partition is not None else random.randint(0, 9),
            offset=random.randint(0, 9999) if offset is None else offset,
            tp_start=int(time.time() * 1_000.0),
        )
        return Scenario.expected_when_consumed(
            scenario_item,
            topic=f"topic-{random.randint(0, 9999)}" if topic is None else topic,
        )


def generate_random_payload_to_produce() -> dict:
    headers = []
    for _ in range(random.randint(0, 3)):
        header_key: str = f"key-{randint(1, 3)}"
        header_value: bytes = str(uuid4()).encode("utf-8")
        headers.append(
            (
                header_key,
                header_value,
            )
        )

    key: bytes = f"office:{randint(1, 5)}".encode()
    value: bytes = json.dumps({"account_id": randint(1, 50)}).encode("utf-8")

    payload: dict[str, object] = dict(key=key, value=value)
    if headers:
        payload["headers"] = headers

    return payload


def generate_random_big_binary_payload_to_produce() -> dict:
    headers = []
    for _ in range(random.randint(0, 5)):
        headers.append(
            (
                f"some-header-key-{randint(1, 5)}",
                random.randbytes(randint(32, 256)),
            )
        )

    payload: dict[str, object] = dict(
        key=random.randbytes(randint(64, 128)), value=random.randbytes(randint(8 * 1024, 16 * 1024))
    )
    if headers:
        payload["headers"] = headers

    return payload


ScenarioItem = namedtuple("ScenarioItem", ["payload_to_produce", "expected_when_consumed"])


@dataclasses.dataclass
class Scenario:
    all_events: list[ScenarioItem]
    partition_0: list[ScenarioItem]
    partition_1: list[ScenarioItem]
    partition_2: list[ScenarioItem]

    @classmethod
    def create_scenario_item(cls, partition: int, offset: int, tp_start: int):
        payload_to_produce = generate_random_payload_to_produce()
        payload_to_produce["partition"] = partition
        expected_when_consumed_data = {"offset": offset, "ts": tp_start + offset}
        return ScenarioItem(payload_to_produce, expected_when_consumed_data)

    @classmethod
    def create(cls, num_messages=None) -> Scenario:
        num_messages = num_messages or int(os.environ.get("NUM_MESSAGES", "1000"))

        partition_0 = [cls.create_scenario_item(0, _, int(time.time() * 1_000.0)) for _ in range(int(num_messages / 3))]
        partition_1 = [cls.create_scenario_item(1, _, int(time.time() * 1_000.0)) for _ in range(int(num_messages / 3))]
        partition_2 = [
            cls.create_scenario_item(2, _, int(time.time() * 1_000.0))
            for _ in range(num_messages - (len(partition_0) + len(partition_1)))
        ]
        all_events = partition_0 + partition_1 + partition_2
        scenario = Scenario(
            all_events=all_events,
            partition_0=partition_0,
            partition_1=partition_1,
            partition_2=partition_2,
        )
        assert len(scenario.all_events) == len(scenario.partition_0) + len(scenario.partition_1) + len(
            scenario.partition_2
        )
        return scenario

    @classmethod
    def expected_when_consumed(cls, item: ScenarioItem, topic: str):
        return KafkaConsumedMessageMock(
            mock_topic=topic,
            mock_partition=item.payload_to_produce["partition"],
            mock_offset=item.expected_when_consumed["offset"],
            mock_timestamp=(TIMESTAMP_CREATE_TIME, item.expected_when_consumed["ts"]),
            mock_key=item.payload_to_produce["key"],
            mock_value=item.payload_to_produce["value"],
            mock_headers=item.payload_to_produce.get("headers", None),
        )
