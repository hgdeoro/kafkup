import json
import re
import subprocess
from collections.abc import Callable

from tests.conftest import kafka_required
from tests.kafka_mock import Scenario


@kafka_required
def test_kafkacat_works(topic: str):
    cp = subprocess.run(["kafkacat", "-b", "localhost:9092", "-L"], capture_output=True)
    assert re.search(r"broker 1 at \S+ \(controller\)", cp.stdout.decode("utf-8"))


@kafka_required
def test_get_topics(get_topics: Callable):
    topics = get_topics()
    assert "__consumer_offsets" in topics


@kafka_required
def test_topic_is_created(topic: str):
    assert topic
    cp = subprocess.run(["kafkacat", "-b", "localhost:9092", "-L", "-t", topic], capture_output=True)
    assert re.search(rf'topic "{topic}" with 3 partitions', cp.stdout.decode("utf-8"))


@kafka_required
def test_populator(topic: str, populator):
    assert topic
    populator(topic, Scenario.create(num_messages=10))
    cp = subprocess.run(["kafkacat", "-b", "localhost:9092", "-C", "-t", topic, "-e"], capture_output=True)
    messages = cp.stdout.decode("utf-8").splitlines()
    assert len(messages) > 0, "topic is empty"
    assert all([True for msg in messages if json.loads(msg)])


@kafka_required
def test_kafkacat_dump(topic: str, populator, kafkacat_dump: Callable):
    assert topic
    scenario = Scenario.create(num_messages=10)
    populator(topic, scenario)
    dump = kafkacat_dump(topic)
    assert len(dump) == 10
    assert [_.payload_to_produce["key"].decode("utf-8") for _ in scenario.partition_0] == [
        _["key"] for _ in dump if _["partition"] == 0
    ]
    assert [_.payload_to_produce["key"].decode("utf-8") for _ in scenario.partition_1] == [
        _["key"] for _ in dump if _["partition"] == 1
    ]
    assert [_.payload_to_produce["key"].decode("utf-8") for _ in scenario.partition_2] == [
        _["key"] for _ in dump if _["partition"] == 2
    ]


def test_scenarios():
    assert len(Scenario.create(num_messages=5).all_events) == 5
