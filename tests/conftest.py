from __future__ import annotations

import json
import logging
import os
import subprocess
import time
from collections.abc import Callable
from collections.abc import Iterable
from uuid import uuid4

import boto3
import pytest
from confluent_kafka import Consumer
from confluent_kafka import Producer
from confluent_kafka.admin import AdminClient
from confluent_kafka.admin import NewTopic

from kafkup.block import KAFKA_MESSAGE_TUPLE_OFFSET
from kafkup.block import KAFKA_MESSAGE_TUPLE_PARTITION
from kafkup.block import KAFKA_MESSAGE_TUPLE_TOPIC
from kafkup.block import Block
from kafkup.block import BlockData
from kafkup.block import BlockMetadata
from kafkup.block import KafkaMessageTuple
from kafkup.block import TopicPartition
from kafkup.block import as_tuple
from kafkup.block_builder import BlockBuilder
from kafkup.configuration import Configuration
from tests.kafka_mock import KafkaConsumedMessageMock
from tests.kafka_mock import Scenario
from tests.kafka_mock import ScenarioItem

logger = logging.getLogger(__name__)


def _port_open(port: int):
    import socket

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = sock.connect_ex(("127.0.0.1", port))
    sock.close()
    return result == 0


kafka_required = pytest.mark.skipif(not _port_open(9092), reason="kafka running on localhost:9092 is required")
minio_required = pytest.mark.skipif(not _port_open(9000), reason="minio running on localhost:9000 is required")


@pytest.fixture
def kafka_admin(configuration):
    return AdminClient(configuration.kafka_config)


@pytest.fixture
def get_topics(kafka_admin) -> Callable:
    def func() -> Iterable:
        def inner_func():
            cluster_metadata = kafka_admin.list_topics()
            for topic, metadata in cluster_metadata.topics.items():
                yield topic

        return list(inner_func())

    return func


@pytest.fixture
def kafka_consumer(configuration: Configuration):
    return Consumer(configuration.kafka_config)


@pytest.fixture
def kafka_producer(create_kafka_producer):
    return create_kafka_producer()


@pytest.fixture
def create_kafka_producer(configuration: Configuration):
    def func():
        return Producer(configuration.kafka_config)

    return func


@pytest.fixture
def create_topic(kafka_admin, get_topics: Callable):
    def func(config=None) -> str:
        new_topic = f"test-{uuid4()}"
        assert new_topic not in get_topics()

        logger.debug("Creating new topic for current run. new_topic=%s", new_topic)
        ret = kafka_admin.create_topics([NewTopic(topic=new_topic, num_partitions=3, config=config or {})])
        assert ret[new_topic].result() is None

        start = time.monotonic()
        while time.monotonic() - start < 10:
            if new_topic in get_topics():
                logger.debug("New topic found, continuing... new_topic=%s", new_topic)
                return new_topic
            logger.debug("Created topic still not found. new_topic=%s", new_topic)
        raise Exception(f"The topic that we just created couldn't be found. new_topic={new_topic}")

    return func


@pytest.fixture(scope="function")
def topic(create_topic) -> str:
    force_topic = os.environ.get("FORCE_TOPIC", "").strip()
    if force_topic:
        logger.info("FORCE_TOPIC was set, won't create")
        return force_topic
    return create_topic()


@pytest.fixture
def populator(kafka_admin, kafka_producer: Producer):
    def delivery_report(err, msg):
        if err is None:
            logger.debug("Message delivered. msg=%s, topic=%s, offset=%s", msg, msg.topic(), msg.offset())
        else:
            raise Exception(f"Message not delivered: {err}")

    def populate(topic: str, scenario: Scenario):
        force_topic = os.environ.get("FORCE_TOPIC", "").strip()
        if force_topic:
            logger.info("FORCE_TOPIC was set, won't populate")
            return

        logger.info("Start population of topic %s...", topic)

        scenario_item: ScenarioItem
        for scenario_item in scenario.all_events:
            # Trigger any available delivery report callbacks from previous produce() calls
            kafka_producer.poll(0)

            # Asynchronously produce a message. The delivery report callback will
            # be triggered from the call to poll() above, or flush() below, when the
            # message has been successfully delivered or failed permanently.
            assert "partition" in scenario_item.payload_to_produce
            kafka_producer.produce(topic=topic, callback=delivery_report, **scenario_item.payload_to_produce)

            # Wait for any outstanding messages to be delivered and delivery report callbacks to be triggered.
            kafka_producer.flush()

        kafka_producer.poll(0)
        kafka_producer.flush()

        logger.info("Population of topic %s finished", topic)

        return [
            TopicPartition(
                topic=topic,
                partition=0,
            ),
            TopicPartition(
                topic=topic,
                partition=1,
            ),
            TopicPartition(
                topic=topic,
                partition=2,
            ),
        ]

    return populate


@pytest.fixture
def kafkacat_dump():
    def func(topic) -> list[dict]:
        cmd = (
            f"( "
            f"  kafkacat -b localhost:9092 -C -e -J -t {topic} -p 0 ; "
            f"  kafkacat -b localhost:9092 -C -e -J -t {topic} -p 1 ; "
            f"  kafkacat -b localhost:9092 -C -e -J -t {topic} -p 2 "
            f") | jq -s"
        )
        cp = subprocess.run(cmd, capture_output=True, shell=True)
        return json.loads(cp.stdout.decode("utf-8"))

    # [
    #   {
    #     "topic": "test-87e5ff9d-a38b-4be1-b1f6-5ab7afb0bcef",
    #     "partition": 0,
    #     "offset": 0,
    #     "tstype": "create",
    #     "ts": 1688721456142,
    #     "broker": -1,
    #     "headers": [
    #       "key-2",
    #       "5d32b782-712d-4628-b2f7-8baf6f511881",
    #       "key-2",
    #       "b721feb0-dcc8-4a21-872a-3ededde84f97",
    #       "key-2",
    #       "1566d339-8736-4519-b761-c45085355ced",
    #       "key-3",
    #       "acdbf022-1efe-4200-aaa9-8eb9ffd95711",
    #       "key-1",
    #       "aabcea6a-aab0-4013-b7d1-f7ed034e3242"
    #     ],
    #     "key": "office:3",
    #     "payload": "{\"account_id\": 39, \"num\": 0}"
    #   },
    #   ...
    # ]

    return func


@pytest.fixture
def random_kafka_consumed_message():
    def func():
        return KafkaConsumedMessageMock.random()

    return func


@pytest.fixture
def random_block(random_kafka_consumed_message: Callable):
    def func(prev_block: Block | None = None, num_events: int = 10):
        if prev_block is None:
            events: list[KafkaMessageTuple] = [as_tuple(random_kafka_consumed_message()) for _ in range(num_events)]
            data = BlockData(events=events)
            end = {
                # FIXME: this is WRONG `metadata.end`, but at least is not empty
                TopicPartition(
                    topic=event[KAFKA_MESSAGE_TUPLE_TOPIC],  # type: ignore
                    partition=event[KAFKA_MESSAGE_TUPLE_PARTITION],  # type: ignore
                ): event[KAFKA_MESSAGE_TUPLE_OFFSET]
                for event in events
            }
            # FIXME: add `metadata.counters`
            metadata = BlockMetadata(start={}, end=end, counters={}, block_number=0)  # type: ignore
            return Block(data=data, metadata=metadata)
        raise NotImplementedError()

    return func


@pytest.fixture
def s3_bucket():
    # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3/bucket/index.html
    s3 = boto3.resource(
        "s3",
        endpoint_url="http://127.0.0.1:9000",
        config=boto3.session.Config(signature_version="s3v4"),
        aws_access_key_id="miniouser",
        aws_secret_access_key="miniopass",
    )
    bucket_name = "kafkup"
    return s3.Bucket(bucket_name)


@pytest.fixture
def configuration() -> Configuration:
    kafka_config = {
        "bootstrap.servers": "localhost:9092",
        "socket.connection.setup.timeout.ms": 1000,
        "group.id": f"random-group-{uuid4().hex}",
        "auto.offset.reset": "earliest",
    }
    return Configuration(bucket="kafkup", backup_name=str(uuid4()), max_messages_per_block=5, kafka_config=kafka_config)


@pytest.fixture
def block_chain_generate() -> Callable:
    def func(configuration: Configuration, scenario_size: int, block_builder: BlockBuilder):
        # configuration = configuration.with_overrides(max_messages_per_block=10)
        scenario = Scenario.create(scenario_size)
        blocks = []
        for scenario_item in scenario.all_events:
            event = Scenario.expected_when_consumed(scenario_item, topic="dummy-topic")
            block = block_builder.handle_event(event)
            if block:
                blocks.append(block)

        return blocks

    return func
