PYTHON ?= python3.11
VENVDIR ?= $(abspath ./venv)

export PATH := $(VENVDIR)/bin:$(PATH)

# Rust

#lint:
#	cargo fmt
#
#test: lint
#	cargo test
#
#run: lint
#	#env RUST_LOG="debug,librdkafka=trace,rdkafka::client=debug" cargo run
#	env RUST_LOG="debug" cargo run
#
#build: lint
#	cargo build
#
#release:
#	cargo build --release

# Python

venv:
	$(PYTHON) -m venv venv
	$(VENVDIR)/bin/pip install pip-tools

pip-compile:
	$(VENVDIR)/bin/pip-compile -o reqs/requirements-prod.txt reqs/requirements-prod.in
	$(VENVDIR)/bin/pip-compile -o reqs/requirements-dev.txt  reqs/requirements-dev.in

pip-sync:
	$(VENVDIR)/bin/pip-sync reqs/requirements-prod.txt reqs/requirements-dev.txt

pip-compile-sync: pip-compile pip-sync

# Linting

pylint:
	$(VENVDIR)/bin/pylint -f colorized kafkup/

pre-commit:
	$(VENVDIR)/bin/pre-commit run -a

# Testing

pytest:
	env RUN_SYSTEM_TESTS=false \
		$(VENVDIR)/bin/pytest -vs tests/

pytest-debug:
	env RUN_SYSTEM_TESTS=false \
		$(VENVDIR)/bin/pytest -vs --log-cli-level=DEBUG tests/

coverage:
	$(VENVDIR)/bin/coverage run $(VENVDIR)/bin/pytest tests/
	$(VENVDIR)/bin/coverage report --skip-empty

coverage-html:
	$(VENVDIR)/bin/coverage run $(VENVDIR)/bin/pytest tests/
	$(VENVDIR)/bin/coverage html --skip-empty --omit='tests/*'
	open htmlcov/index.html

# Docker compose

docker-compose-up:
	$(VENVDIR)/bin/docker-compose up

docker-compose-up-clean:
	$(VENVDIR)/bin/docker-compose kill
	$(VENVDIR)/bin/docker-compose rm -f
	docker volume rm -f kafkup_kafka_data
	docker volume rm -f kafkup_minio_storage
	$(VENVDIR)/bin/docker-compose up
