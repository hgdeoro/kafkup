# Kafkup

A simple tool for backing up Kafka topics.

## Scope

Create backup of topics of a Kafka cluster to S3.

### Design philosophy

* Simplicity of software design
* Cost efficiency (might not be perfect)
* Optimized for backing up (assumes restore are not usual)
* Incremental backup can be verified

### Out of scope

* Real-time data duplication (if you need that, use MirrorMaker)
* Automation of recovery

# Design

```mermaid
sequenceDiagram
    participant MainProcess
    participant PartitionDiscoveryBase
    participant PartitionReaderProcess
    participant BlockBuilder
    participant Block
    participant BlockStorage
    participant Kafka
    participant S3

    MainProcess->>PartitionDiscoveryBase: start()
    PartitionDiscoveryBase->>Kafka: get_topics_metadata()
    MainProcess->>PartitionReaderProcess: start()
    PartitionReaderProcess->>BlockStorage: get_last_block_metadata()
    BlockStorage->>S3: get()
    PartitionReaderProcess->>Kafka: assign()
    PartitionReaderProcess->>Kafka: poll()
    PartitionReaderProcess->>BlockBuilder: handle_event()
    BlockBuilder->>Block: new()
    PartitionReaderProcess->>BlockStorage: upload(block)
    BlockStorage->>S3: upload()
```

### Components

* MainProcess: initial process that handle child processes running in parallel.
* PartitionDiscoveryBase: polls Kafka cluster for partitions and informs
  when a change occurs (changes would be: deleted topic, new topic new partition).
* PartitionReaderBase: reads data from Kafka and pass events to `BlockBuilder`.
* BlockBuilder: build blocks with events received, and uploads them to S3.
* BlockStorage: handles upload to and downloads from storage (basically, an S3 bucket).
* Block: the unit that is uploaded into S3. Contains 2 parts: data (messages from Kafka) and metadata.

The most important component follows the open-close principle:

```mermaid
classDiagram
    BlockBuilder <|-- InMemoryBlockBuilder
    BlockStorage <|-- S3BlockStorage
```

#### Block

A block is implemented as 2 files to be uploaded to S3. Data (bytes read from partitions, some binary
format) and metadata (a dump of all the partitions that are being included in
the backup, with the latest offset being backed up, in JSON format, we don't need
to be efficient here, JSON will be easy to read for a human).

Inspired by blockchain, with each block we include a reference to the previous block. This
is simple to implement and will gives a way to handle at least 2 situations:

1. how to make sure that all required blocks exists. Since the backup is
   of incremental nature, it will be a valid backup only if all blocks are available.
   By doing this chain of blocks, we can verify if a backup is complete or not.
2. our design survives when 2 processes are doing the same backup, because some error or
   because 2 versions are running at the same time as part of a rolling upgrade.

_Note that because each block doesn't yet include a reference to the previous block,
current implementation doesn't support all the features described above._

At start time, we pick the last block, and use the offsets from metadata to
start reading from there.

We need the `OrphanBlockCleaner` to remove orphan blocks, but this can be
implemented in a future iteration.

##### Example run

Block 0:

    +----------------------+-------+
    | Name                 | Value |
    +----------------------+-------+
    | Block number         |     0 |
    | # messages in block  |   100 |
    | # partitions @ start |     0 |
    | # partitions @ end   |     1 |
    +----------------------+-------+
    +-------------------------------------------+-----------+-------+--------+-------+
    | Topic                                     | Partition | Start |    End | Count |
    +-------------------------------------------+-----------+-------+--------+-------+
    | test-06ea00fc-4d28-4dc9-821c-3d6ed64ed647 |         2 |       | 150432 |   100 |
    +-------------------------------------------+-----------+-------+--------+-------+

Block 1:

    +----------------------+-------+
    | Name                 | Value |
    +----------------------+-------+
    | Block number         |     1 |
    | # messages in block  |   100 |
    | # partitions @ start |     1 |
    | # partitions @ end   |     2 |
    +----------------------+-------+
    +-------------------------------------------+-----------+--------+--------+-------+
    | Topic                                     | Partition |  Start |    End | Count |
    +-------------------------------------------+-----------+--------+--------+-------+
    | test-06ea00fc-4d28-4dc9-821c-3d6ed64ed647 |         1 |        | 150362 |    45 |
    | test-06ea00fc-4d28-4dc9-821c-3d6ed64ed647 |         2 | 150432 | 150487 |    55 |
    +-------------------------------------------+-----------+--------+--------+-------+

Block 2:

    +----------------------+-------+
    | Name                 | Value |
    +----------------------+-------+
    | Block number         |     2 |
    | # messages in block  |   100 |
    | # partitions @ start |     2 |
    | # partitions @ end   |     3 |
    +----------------------+-------+
    +-------------------------------------------+-----------+--------+--------+-------+
    | Topic                                     | Partition |  Start |    End | Count |
    +-------------------------------------------+-----------+--------+--------+-------+
    | test-06ea00fc-4d28-4dc9-821c-3d6ed64ed647 |         0 |        | 150901 |    62 |
    | test-06ea00fc-4d28-4dc9-821c-3d6ed64ed647 |         1 | 150362 | 150400 |    38 |
    | test-06ea00fc-4d28-4dc9-821c-3d6ed64ed647 |         2 | 150487 | 150487 |       |
    +-------------------------------------------+-----------+--------+--------+-------+

Block 3:

    +----------------------+-------+
    | Name                 | Value |
    +----------------------+-------+
    | Block number         |     3 |
    | # messages in block  |   100 |
    | # partitions @ start |     3 |
    | # partitions @ end   |     3 |
    +----------------------+-------+
    +-------------------------------------------+-----------+--------+--------+-------+
    | Topic                                     | Partition |  Start |    End | Count |
    +-------------------------------------------+-----------+--------+--------+-------+
    | test-06ea00fc-4d28-4dc9-821c-3d6ed64ed647 |         0 | 150901 | 150927 |    26 |
    | test-06ea00fc-4d28-4dc9-821c-3d6ed64ed647 |         1 | 150400 | 150474 |    74 |
    | test-06ea00fc-4d28-4dc9-821c-3d6ed64ed647 |         2 | 150487 | 150487 |       |
    +-------------------------------------------+-----------+--------+--------+-------+

Block 4:

    +----------------------+-------+
    | Name                 | Value |
    +----------------------+-------+
    | Block number         |     4 |
    | # messages in block  |   100 |
    | # partitions @ start |     3 |
    | # partitions @ end   |     3 |
    +----------------------+-------+
    +-------------------------------------------+-----------+--------+--------+-------+
    | Topic                                     | Partition |  Start |    End | Count |
    +-------------------------------------------+-----------+--------+--------+-------+
    | test-06ea00fc-4d28-4dc9-821c-3d6ed64ed647 |         0 | 150927 | 150976 |    49 |
    | test-06ea00fc-4d28-4dc9-821c-3d6ed64ed647 |         1 | 150474 | 150474 |       |
    | test-06ea00fc-4d28-4dc9-821c-3d6ed64ed647 |         2 | 150487 | 150538 |    51 |
    +-------------------------------------------+-----------+--------+--------+-------+


#### Multiple backups

Each backup is identified by the directory name, in the root of the S3 bucket.

By supporting multiple backups per bucket, we can "rotate" the backups. Rotating
the backups is important because:

1. we don't want to keep historical data, only a timeframe (for example, 2 last weeks)
2. we don't want to keep doing backup of compacted topics and store old data forever.

Rotating backups is not efficient in terms of used space, but is simple mechanism to implement.

#### Contents on S3

If we look at the bucket, each buckup looks like:

    /backup-name/data-0-5694d415177746e9ba9560a37e46b534.pickle
    /backup-name/metadata-0-5694d415177746e9ba9560a37e46b534.json
    /backup-name/data-1-e9099237e96543acac4a79add54cb57a.pickle
    /backup-name/metadata-1-e9099237e96543acac4a79add54cb57a.json
    (...)


# ADR snippets


### ADR-1: `block.start` is filled incrementally

I wonder if when a new backup is created, the initial block should have an
empty `block.start`, or a `block.start` with the offsets of all the existing partitions?
For now, let's try an empty `block.start` for the initial block, and make `block.start` grow
from block to block.

### ADR-2: behaviour with deleted topics is undefined

The `block.end` includes known offset of all partitions (regardless if the
blocks contains or not messages from those partitions). But, what should we do with topics
that are deleted? Should we remove them from `block.end`? Should we mark them as deleted?
How we handle the situation where a topic is deleted and then created again with same name?
(see KIP-516: Topic Identifiers)


### ADR-3: backup verifier and clean up

```mermaid
sequenceDiagram
    participant BackupChecker
    participant OrphanBlockCleaner
    participant S3

    BackupChecker->>S3: raad_blocks()
    BackupChecker->>S3: mark_blocks_as_checked()
    OrphanBlockCleaner->>S3: remove_orphans()
```


# Performance

There is a lot of room for improvement. On current development environment I got >52k msg/sec, which is around 600 MiB/sec.
To get those values in local tests, some fine-tuning might be required, for example `fetch.queue.backoff.ms=50`

See `PERFORMANCE.txt` for details.

### Environment

* AMD Ryzen 7 5800HS with Radeon Graphics + 16GB RAM
* Single Kafka 3.5 broker running on Docker
* Internal SSD (but without actual I/O happening, data is served from cache)
* Python 3.11.0rc2 (Ubuntu 22.10)
* confluent-kafka==2.2.0
* Data generated with `generate_random_big_binary_payload_to_produce()`

Best 3 results (after micro-benchmarking, tuning consumer configuration, refactoring):

for consuming data, peak at 2.9 GiB/sec:

    PERFORMANCE_TEST=consumption: using NoOpBlockStorage, NoOpBlockBuilder
    Try #:Status 	messages=419000	size=5.0 GiB	time=1.8	throughput=230845 msg/sec	throughput=2.8 GiB/sec
    Try #:Status 	messages=419000	size=5.0 GiB	time=1.7	throughput=241614 msg/sec	throughput=2.9 GiB/sec
    Try #:Status 	messages=419000	size=5.0 GiB	time=1.8	throughput=234094 msg/sec	throughput=2.8 GiB/sec

for consuming and serializing, peak at 933.6 MiB/sec:

    PERFORMANCE_TEST=serialization: using PatchedS3BlockStorage, NoOpBlockBuilder+InMemoryBlockBuilder
    Try #:Status 	messages=419000	size=5.0 GiB	time=5.5	throughput=76302 msg/sec	throughput=933.6 MiB/sec
    Try #:Status 	messages=419000	size=5.0 GiB	time=5.7	throughput=73706 msg/sec	throughput=901.8 MiB/sec
    Try #:Status 	messages=419000	size=5.0 GiB	time=5.6	throughput=74574 msg/sec	throughput=912.5 MiB/sec
