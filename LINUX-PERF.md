# How to use Linux `perf` profiler

Build `librdkafka`: https://github.com/confluentinc/librdkafka#build-from-source

    kafkup> $ mkdir local
    kafkup> $ git clone https://github.com/confluentinc/librdkafka.git local/librdkafka-src
    kafkup> $ cd local/librdkafka-src
    librdkafka-src> $ git checkout tags/v2.2.0
    librdkafka-src> $ ./configure --prefix=$(pwd)/../librdkafka
    librdkafka-src> $ make -j8
    librdkafka-src> $ make install
    librdkafka-src> $ cd ../..

Build Python 3.12.0b2 that works with `perf`:

    kafkup> $ env CFLAGS="-fno-omit-frame-pointer -mno-omit-leaf-frame-pointer" pyenv install 3.12.0b2
    kafkup> $ pyenv global 3.12.0b2 # enable 3.12.0b2
    kafkup> $ pyenv global system   # set default back to default python version in the system
    kafkup> $ python3.12 --version
    Python 3.12.0b2


Build venv & install dependencies:

    kafkup> $ python3.12 -m venv venv3.12
    kafkup> $ source venv3.12/bin/activate
    kafkup> $ cat reqs/requirements-prod.in reqs/requirements-dev.in | grep -v requirements-prod.txt > local/reqs-tmp.txt
    kafkup> $ env CFLAGS=-I$(pwd)/local/librdkafka/include  \
                LDFLAGS=-L$(pwd)/local/librdkafka/lib \
                    pip install -r local/reqs-tmp.txt

If you get the error `AttributeError: cython_sources`, you'll need a workaround to install PyYAML,
see `local/reqs-tmp.txt` for potential solutions.

Now `perf` can be used to profile Python projects.

# Profile consumption+serialization of messages

### Setup environment variables

    kafkup> $ source venv3.12/bin/activate
    kafkup> $ export LD_LIBRARY_PATH=$(pwd)/local/librdkafka/lib

### Create a topic and populate with data

    kafkup> $ export KAFKUP_CONFIGURATION_FILE=config.json
    kafkup> $ env KAFKUP_LOGGING_LEVEL=info \
                DATA_GENERATOR_FUNC=generate_random_big_binary_payload_to_produce \
                    python3 -m kafkup.cli.synthetic_create_and_populate

### Let's run the profiler

    kafkup> $ export TOPIC=<name of the topic created in previous step>
    kafkup> $ export BLOCK_BUILDER_BYTES_LIMIT=$(( 1024 * 1024 * 512 ))
    kafkup> $ perf record -F 9999 --strict-freq -g -o perf.data \
                env PYTHONPERFSUPPORT=1 \
                    KAFKUP_LOGGING_LEVEL=warn \
                    KAFKUP_BACKUP_NAME=from-cli-$(date +%s) \
                    TOPIC_PARTITIONS=$TOPIC:0,$TOPIC:1,$TOPIC:2 \
                    KAFKUP_MAX_MESSAGES_PER_BLOCK=1000 \
                    PERFORMANCE_TEST=serialization \
                        python3 -u -m kafkup.cli.partition_reader_performance

### Let's see the report

For an interactive visualization:

    kafkup> $ perf report -i perf.data

You will get an interactive interface:

    Samples: 19K of event 'cycles', Event count (approx.): 7676380661
      Children      Self  Command       Shared Object                                 Symbol
    +   64.42%     8.16%  python3       libpython3.12.so.1.0                          [.] _PyEval_EvalFrameDefault
    +   64.32%     0.26%  python3       libpython3.12.so.1.0                          [.] PyObject_Vectorcall
    +   63.08%     0.02%  python3       libpython3.12.so.1.0                          [.] cfunction_vectorcall_FASTCALL_KEYWORDS
    +   62.69%     0.01%  python3       libpython3.12.so.1.0                          [.] PyEval_EvalCode
    +   62.67%     0.00%  python3       libpython3.12.so.1.0                          [.] builtin_exec
    +   58.29%     0.00%  python3       libpython3.12.so.1.0                          [.] Py_BytesMain
    +   58.24%     0.00%  python3       libc.so.6                                     [.] __libc_start_call_main
    +   57.95%     0.00%  python3       libpython3.12.so.1.0                          [.] Py_RunMain
    +   55.48%     0.00%  python3       libpython3.12.so.1.0                          [.] pymain_run_module
    +   55.48%     0.00%  python3       [JIT] tid 111678                              [.] py::_run_module_as_main:<frozen runpy>
    +   55.25%     0.00%  python3       [JIT] tid 111678                              [.] py::<module>:/home/user/kafkup/kafkup/cli/partition_reader_performance.py

Or you can get the complet context in stdoutput:

    kafkup> $ perf report -i perf.data -g flat,10,1 | head -n 50
    # To display the perf.data header info, please use --header/--header-only options.
    #
    #
    # Total Lost Samples: 0
    #
    # Samples: 19K of event 'cycles'
    # Event count (approx.): 7676380661
    #
    # Children      Self  Command       Shared Object                                 Symbol
    # ........  ........  ............  ............................................  .................................................................................................................................................................................
    #
        64.42%     8.16%  python3       libpython3.12.so.1.0                          [.] _PyEval_EvalFrameDefault
        64.32%     0.26%  python3       libpython3.12.so.1.0                          [.] PyObject_Vectorcall
        63.08%     0.02%  python3       libpython3.12.so.1.0                          [.] cfunction_vectorcall_FASTCALL_KEYWORDS
        62.69%     0.01%  python3       libpython3.12.so.1.0                          [.] PyEval_EvalCode
        62.67%     0.00%  python3       libpython3.12.so.1.0                          [.] builtin_exec
        58.29%     0.00%  python3       libpython3.12.so.1.0                          [.] Py_BytesMain
        58.24%     0.00%  python3       libc.so.6                                     [.] __libc_start_call_main
        57.95%     0.00%  python3       libpython3.12.so.1.0                          [.] Py_RunMain
        55.48%     0.00%  python3       libpython3.12.so.1.0                          [.] pymain_run_module
        55.48%     0.00%  python3       [JIT] tid 111678                              [.] py::_run_module_as_main:<frozen runpy>
        55.25%     0.00%  python3       [JIT] tid 111678                              [.] py::<module>:/home/user/kafkup/kafkup/cli/partition_reader_performance.py
        54.92%     0.00%  python3       [JIT] tid 111678                              [.] py::_run_code:<frozen runpy>
        52.81%     0.00%  python3       [JIT] tid 111678                              [.] py::main:/home/user/kafkup/kafkup/cli/partition_reader_performance.py
        52.60%     0.00%  python3       [JIT] tid 111678                              [.] py::PartitionReaderProcess.run:/home/user/kafkup/kafkup/partition_reader.py
        52.53%     0.00%  python3       [JIT] tid 111678                              [.] py::PartitionReaderProcess._run:/home/user/kafkup/kafkup/partition_reader.py
        23.82%     0.00%  python3       [JIT] tid 111678                              [.] py::NoOpBlockBuilder.handle_event:/home/user/kafkup/kafkup/cli/partition_reader_performance.py
        20.00%     0.00%  python3       [JIT] tid 111678                              [.] py::InMemoryBlockBuilder.handle_event:/home/user/kafkup/kafkup/block_builder.py
        15.65%     0.04%  python3       [kernel.kallsyms]                             [k] entry_SYSCALL_64_after_hwframe
        15.53%     0.02%  python3       [kernel.kallsyms]                             [k] do_syscall_64
        14.57%    10.50%  python3       libc.so.6                                     [.] __memmove_avx_unaligned_erms
        13.12%     0.00%  python3       [JIT] tid 111678                              [.] py::PartitionReaderProcess._on_new_block_finished:/home/user/kafkup/kafkup/partition_reader.py
        13.05%     0.00%  python3       [vdso]                                        [.] 0x00007ffe0833c8bd
        13.00%     0.00%  python3       [JIT] tid 111678                              [.] py::S3BlockStorage.serialize_and_upload:/home/user/kafkup/kafkup/block_storage.py
        12.85%     0.05%  python3       libc.so.6                                     [.] clock_gettime@@GLIBC_2.17
        12.77%     0.00%  python3       _pickle.cpython-312-x86_64-linux-gnu.so       [.] _pickle_dumps
        12.36%     0.01%  rdk:broker1   [kernel.kallsyms]                             [k] entry_SYSCALL_64_after_hwframe
        12.35%     0.01%  rdk:broker1   [kernel.kallsyms]                             [k] do_syscall_64
        12.17%     0.12%  python3       [kernel.kallsyms]                             [k] __x64_sys_clock_gettime
        12.00%     1.50%  python3       _pickle.cpython-312-x86_64-linux-gnu.so       [.] save.constprop.0
        12.00%     0.00%  python3       _pickle.cpython-312-x86_64-linux-gnu.so       [.] dump
        11.98%     0.10%  python3       _pickle.cpython-312-x86_64-linux-gnu.so       [.] save_reduce
        11.95%     0.00%  python3       _pickle.cpython-312-x86_64-linux-gnu.so       [.] save_dict
        11.94%     0.45%  python3       _pickle.cpython-312-x86_64-linux-gnu.so       [.] save_list
        11.66%    11.64%  python3       [kernel.kallsyms]                             [k] read_hpet
        11.57%     0.00%  python3       [JIT] tid 111678                              [.] py::IncrementalBlockMetadata.update:/home/user/kafkup/kafkup/block_builder.py
        11.25%     0.01%  rdk:broker1   libc.so.6                                     [.] __libc_recvmsg
        11.20%     0.00%  rdk:broker1   [kernel.kallsyms]                             [k] __x64_sys_recvmsg
        11.20%     0.01%  rdk:broker1   [kernel.kallsyms]                             [k] __sys_recvmsg
        11.18%     0.01%  rdk:broker1   [kernel.kallsyms]                             [k] ___sys_recvmsg
