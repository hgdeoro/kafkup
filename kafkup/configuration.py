import dataclasses
import json
import logging
import os
import pathlib


@dataclasses.dataclass(frozen=True)
class Configuration:
    PRETTY_PRINT_LEVEL_DISABLED = 0
    PRETTY_PRINT_LEVEL_ENABLED = 1
    PRETTY_PRINT_LEVEL_DETAILED = 2

    bucket: str
    backup_name: str
    max_messages_per_block: int
    kafka_config: dict
    logging_level: int = logging.INFO
    pretty_print_level: int = PRETTY_PRINT_LEVEL_DISABLED

    @classmethod
    def _from_file_or_env(cls, key: str, config_dict: dict, default=None, required=False) -> str | None:
        if f"KAFKUP_{key}".upper() in os.environ:
            return os.environ[f"KAFKUP_{key}".upper()]

        if key in config_dict:
            return config_dict[key]

        if required:
            raise Exception(f"Configuration for '{key}' required in file or env")
        return default

    @classmethod
    def from_env(cls):
        assert "KAFKUP_CONFIGURATION_FILE" in os.environ, "KAFKUP_CONFIGURATION_FILE not found in environment"
        filename = os.environ["KAFKUP_CONFIGURATION_FILE"]
        file = pathlib.Path(filename)
        assert file.exists()
        config_dict = json.loads(file.read_text())  # pylint: disable=unspecified-encoding

        match cls._from_file_or_env("logging_level", config_dict, default="info").lower():
            case "warn":
                logging_level = logging.WARNING
            case "info":
                logging_level = logging.INFO
            case "debug":
                logging_level = logging.DEBUG
            case _:
                logging_level = logging.INFO

        bucket = cls._from_file_or_env("bucket", config_dict, required=True)
        backup_name = cls._from_file_or_env("backup_name", config_dict, required=True)
        max_messages_per_block = int(cls._from_file_or_env("max_messages_per_block", config_dict, default="100"))
        pretty_print_level = int(cls._from_file_or_env("pretty_print_level", config_dict, default="0"))
        assert cls.PRETTY_PRINT_LEVEL_DISABLED <= pretty_print_level <= cls.PRETTY_PRINT_LEVEL_DETAILED

        kafka_config = config_dict["kafka_config"]

        return Configuration(
            bucket=bucket,
            backup_name=backup_name,
            logging_level=logging_level,
            max_messages_per_block=max_messages_per_block,
            kafka_config=kafka_config,
            pretty_print_level=pretty_print_level,
        )

    def with_overrides(self, **kwargs):
        data = dataclasses.asdict(self)
        invalid_keys = set(kwargs.keys()) - set(data.keys())
        if invalid_keys:
            raise KeyError(f"Invalid configuration keys specified: {invalid_keys}")
        data.update(kwargs)
        return Configuration(**data)
