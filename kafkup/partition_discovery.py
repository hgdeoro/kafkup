import abc
import dataclasses
import logging
import pprint
import time

from kafkup import kafka_client
from kafkup.concurrency import ThreadOrProcess
from kafkup.concurrency import XQueue
from kafkup.configuration import Configuration


class StopProcessException(Exception):
    pass


@dataclasses.dataclass(frozen=True)
class MetadataUpdate:
    partitions_metadata: list[kafka_client.PartitionMetadata]
    changed: bool


class PartitionDiscoveryBase(ThreadOrProcess, abc.ABC):
    SUBCLASS_BASENAME = "PartitionDiscoveryBase"
    logger = logging.getLogger(f"{__name__}.PartitionDiscoveryBase")

    def __init__(
        self,
        *args,
        sleep_between_poll: float | None = None,
        configuration: Configuration,
        **kwargs,
    ):
        assert configuration is not None
        super().__init__(*args, **kwargs)
        self._configuration: Configuration = configuration
        self._metadata_update_queue: XQueue = self._new_queue()
        self._sleep_between_poll = 5 if sleep_between_poll is None else sleep_between_poll
        self._kafka_client = kafka_client.KafkaClient(configuration=self._configuration)

    @property
    def metadata_update_queue(self) -> XQueue:
        return self._metadata_update_queue

    def _do_sleep(self):
        time.sleep(self._sleep_between_poll)

    def _publish_metadata(self, metadata_update: MetadataUpdate):
        if self._metadata_update_queue.full():
            raise Exception("metadata_update_queue is full")
        self._metadata_update_queue.put(metadata_update)

    def run(self) -> None:
        try:
            self._run()
        except StopProcessException:
            logging.warning("StopProcessException received. Stopping...")
            return

    def _run(self) -> None:
        self.logger.info("Starting...")

        metadata = self._kafka_client.get_topics_metadata()
        self.logger.debug("Initial metadata: %s", pprint.pformat(metadata))
        self._publish_metadata(MetadataUpdate(partitions_metadata=metadata.get_partitions_metadata(), changed=True))

        while True:
            new_metadata = self._kafka_client.get_topics_metadata()
            if new_metadata == metadata:
                self.logger.debug("No changes in metadata...")
                self._publish_metadata(
                    MetadataUpdate(partitions_metadata=metadata.get_partitions_metadata(), changed=False)
                )
                self._do_sleep()
            else:
                self.logger.debug("Changes detected in metadata: %s", pprint.pformat(new_metadata))
                metadata = new_metadata
                self._publish_metadata(
                    MetadataUpdate(partitions_metadata=metadata.get_partitions_metadata(), changed=True)
                )
