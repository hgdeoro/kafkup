import logging
import multiprocessing
import queue

from kafkup import partition_discovery
from kafkup import partition_reader
from kafkup.block import TopicPartition
from kafkup.block_builder import InMemoryBlockBuilder
from kafkup.block_storage import S3BlockStorage
from kafkup.configuration import Configuration


class Main:  # pylint: disable=too-few-public-methods
    logger = logging.getLogger(f"{__name__}.Main")

    def __init__(self, configuration: Configuration):
        self._configuration: Configuration = configuration
        self._partition_discovery: partition_discovery.PartitionDiscoveryBase | None = None
        self._partition_reader_process: partition_reader.PartitionReaderBase | None = None

    def _start_partition_discovery(self):
        assert self._partition_discovery is None
        self._partition_discovery = partition_discovery.PartitionDiscoveryBase.as_process()(
            daemon=True, configuration=self._configuration
        )
        self._partition_discovery.start()

    def _check_required_processes_are_running(self):
        self.logger.info(
            "partition_discovery_process[%s].is_alive(): %s",
            self._partition_discovery.identifier(),
            self._partition_discovery.is_alive(),
        )
        assert self._partition_discovery.is_alive()

    def _get_updated_metadata_if_available(self) -> partition_discovery.MetadataUpdate | None:
        try:
            metadata_update = self._partition_discovery.metadata_update_queue.get(timeout=1.0)  # type: ignore # FIXME!
            if metadata_update.changed:
                return metadata_update
            return None
        except queue.Empty:
            return None

    def _start_partition_reader_process(self, metadata: partition_discovery.MetadataUpdate):
        if self._partition_reader_process is not None:
            assert isinstance(self._partition_reader_process, multiprocessing.Process)
            try:
                self._partition_reader_process.kill()  # we know it's a process, we can call `kill`
                self._partition_reader_process.join(timeout=5.0)
                # Note that `join()` returns `None` if its process terminates or if the method times out.
                # Check the process's exitcode to determine if it terminated.
                assert self._partition_reader_process.exitcode is not None, "Process hasn't finished after kill()"
            finally:
                self._partition_reader_process = None

        partitions = [TopicPartition(topic=_.topic, partition=_.partition) for _ in metadata.partitions_metadata]
        self._partition_reader_process = partition_reader.PartitionReaderBase.as_process()(
            daemon=True,
            partitions=partitions,
            block_builder=InMemoryBlockBuilder(configuration=self._configuration),
            block_storage=S3BlockStorage(configuration=self._configuration),
            configuration=self._configuration,
        )
        self._partition_reader_process.start()

    def run(self):
        self._start_partition_discovery()
        assert self._partition_reader_process is None
        while True:
            self._check_required_processes_are_running()
            metadata_update = self._get_updated_metadata_if_available()
            if metadata_update is not None:
                assert metadata_update.changed
                self._start_partition_reader_process(metadata=metadata_update)


def main():
    configuration = Configuration.from_env()
    logging.basicConfig(level=configuration.logging_level)

    main_instance = Main(configuration=configuration)
    main_instance.run()


if __name__ == "__main__":
    main()
