from __future__ import annotations

import dataclasses
from typing import Optional
from typing import TypeAlias

from confluent_kafka import Message as KafkaMessage

KeyType: TypeAlias = Optional[bytes]
ValueType: TypeAlias = Optional[bytes]
HeadersType: TypeAlias = Optional[list[tuple[str, bytes]]]
KafkaMessageTuple: TypeAlias = tuple[str, int, int, tuple[int, int], KeyType, ValueType, HeadersType]

KAFKA_MESSAGE_TUPLE_TOPIC = 0
KAFKA_MESSAGE_TUPLE_PARTITION = 1
KAFKA_MESSAGE_TUPLE_OFFSET = 2
KAFKA_MESSAGE_TUPLE_TIMESTAMP = 3
KAFKA_MESSAGE_TUPLE_KEY = 4
KAFKA_MESSAGE_TUPLE_VALUE = 5
KAFKA_MESSAGE_TUPLE_HEADERS = 6


def as_tuple(kafka_message: KafkaMessage) -> KafkaMessageTuple:
    return (
        kafka_message.topic(),
        kafka_message.partition(),
        kafka_message.offset(),
        kafka_message.timestamp(),
        kafka_message.key(),
        kafka_message.value(),
        kafka_message.headers(),
    )


@dataclasses.dataclass(frozen=True)
class TopicPartition:
    topic: str
    partition: int

    def __post_init__(self):
        assert isinstance(self.topic, str), f"self.topic is {type(self.topic)}"
        assert isinstance(self.partition, int), f"self.partition is {type(self.partition)}"

    def to_str(self) -> str:
        return f"{self.topic}#{self.partition}"

    def from_str(self, topic_partition_string: str) -> TopicPartition:
        topic, partition = topic_partition_string.rsplit("#", maxsplit=1)
        return TopicPartition(topic=topic, partition=int(partition))


@dataclasses.dataclass(frozen=True)
class PartitionOffset:
    topic: str
    partition: int
    offset: int

    def __post_init__(self):
        assert isinstance(self.topic, str), f"self.topic is {type(self.topic)}"
        assert isinstance(self.partition, int), f"self.topic is {type(self.partition)}"
        assert isinstance(self.offset, int), f"self.topic is {type(self.offset)}"


@dataclasses.dataclass(frozen=True)
class BlockMetadata:
    block_number: int
    start: dict[TopicPartition, int]
    end: dict[TopicPartition, int]
    counters: dict[TopicPartition, int]

    def as_dict(self) -> dict:
        return {
            "block_number": self.block_number,
            "start": [[tp.topic, tp.partition, offset] for tp, offset in self.start.items()],
            "end": [[tp.topic, tp.partition, offset] for tp, offset in self.end.items()],
            "counters": [[tp.topic, tp.partition, count] for tp, count in self.counters.items()],
        }

    @classmethod
    def from_dict(cls, data: dict) -> BlockMetadata:
        block_number = data["block_number"]
        start = {TopicPartition(topic=topic, partition=partition): offset for topic, partition, offset in data["start"]}
        end = {TopicPartition(topic=topic, partition=partition): offset for topic, partition, offset in data["end"]}
        counters = {TopicPartition(topic=topic, partition=partition): ct for topic, partition, ct in data["counters"]}
        return BlockMetadata(
            block_number=block_number,
            start=start,
            end=end,
            counters=counters,
        )


def message_tuple_to_dict(message_tuple: KafkaMessageTuple) -> dict[str, object]:
    return dict(
        topic=message_tuple[KAFKA_MESSAGE_TUPLE_TOPIC],
        partition=message_tuple[KAFKA_MESSAGE_TUPLE_PARTITION],
        offset=message_tuple[KAFKA_MESSAGE_TUPLE_OFFSET],
        key=message_tuple[KAFKA_MESSAGE_TUPLE_KEY],
        value=message_tuple[KAFKA_MESSAGE_TUPLE_VALUE],
        timestamp=message_tuple[KAFKA_MESSAGE_TUPLE_TIMESTAMP],
        headers=message_tuple[KAFKA_MESSAGE_TUPLE_HEADERS],
    )


@dataclasses.dataclass(frozen=True)
class BlockData:
    events: list[KafkaMessageTuple]

    def __post_init__(self):
        for _ in self.events:
            assert isinstance(_, tuple)


@dataclasses.dataclass(frozen=True)
class Block:
    metadata: BlockMetadata
    data: BlockData
