import abc
import dataclasses
import io
import json
import logging
import pickle
import pprint
import random
import re
from collections import defaultdict
from collections import namedtuple
from collections.abc import Iterator
from functools import cached_property
from uuid import uuid4

import boto3

from kafkup.block import KAFKA_MESSAGE_TUPLE_OFFSET
from kafkup.block import KAFKA_MESSAGE_TUPLE_PARTITION
from kafkup.block import KAFKA_MESSAGE_TUPLE_TOPIC
from kafkup.block import Block
from kafkup.block import BlockData
from kafkup.block import BlockMetadata
from kafkup.block import TopicPartition
from kafkup.configuration import Configuration
from kafkup.pretty_print import PrettyPrinter


class BackupNotInitializedError(Exception):
    pass


@dataclasses.dataclass
class BlockUploadResult:
    data_path: str  # FIXME: rename to `data_key`
    metadata_path: str  # FIXME: rename to `metadata_key`


class BlockStorage(abc.ABC):
    """
    Handles storge and retrieval of blocks.
    """

    @abc.abstractmethod
    def serialize_and_upload(self, block: Block) -> BlockUploadResult:
        ...

    @abc.abstractmethod
    def get_all_blocks_metadata(self) -> list[BlockMetadata]:
        ...

    @abc.abstractmethod
    def get_latest_block_metadata(self) -> BlockMetadata:
        ...

    @abc.abstractmethod
    def validate(self):
        ...


PathAndParsed = namedtuple("PathAndParsed", ["path", "parsed"])


@dataclasses.dataclass
class ParsedKey:  # pylint: disable=too-many-instance-attributes
    key: str
    parsed: bool
    full_path: str | None = None
    backup_name: str | None = None
    filename: str | None = None
    data_metadata: str | None = None
    block_number: str | None = None
    jitter: str | None = None
    file_ext: str | None = None

    def is_metadata(self):
        return self.parsed and self.data_metadata == "metadata"

    def is_data(self):
        return self.parsed and self.data_metadata == "data"


class S3Client:
    logger = logging.getLogger(f"{__name__}.S3Client")

    def __init__(self, configuration: Configuration):
        assert configuration
        self._configuration = configuration

    @classmethod
    def build_key(  # pylint: disable=too-many-arguments
        cls, backup_name: str, data_metadata: str, block_number: str, jitter: str, file_ext: str
    ) -> str:
        assert "/" not in backup_name
        assert data_metadata in ["data", "metadata"]
        assert re.fullmatch(r"\d+", block_number)
        assert "/" not in jitter
        assert "/" not in file_ext
        return f"{backup_name}/{data_metadata}-{block_number}-{jitter}.{file_ext}"

    @classmethod
    def parse_key(cls, key: str) -> ParsedKey:
        match = re.fullmatch(
            r"""
            ^
            (
                s3://
                (?P<bucket>\w+)
                /
            )?
            (?P<full_path>
                (?P<backup_name>[\w-]+)
                /
                    (?P<filename>
                        (?P<data_metadata>[a-z]+)
                        -
                        (?P<block_number>\d+)
                        -
                        (?P<jitter>\w+)
                        \.
                        (?P<file_ext>[a-z]+)
                    )
            )
            $
            """,
            key,
            re.X,
        )
        if match is None:
            return ParsedKey(key=key, parsed=False)

        group_dict = match.groupdict()
        return ParsedKey(
            key=group_dict["full_path"],  # to make sure it doesn't contain 's3://bucket/'
            parsed=True,
            full_path=group_dict["full_path"],
            backup_name=group_dict["backup_name"],
            filename=group_dict["filename"],
            data_metadata=group_dict["data_metadata"],
            block_number=group_dict["block_number"],
            jitter=group_dict["jitter"],
            file_ext=group_dict["file_ext"],
        )

    @cached_property
    def s3_bucket(self):
        s3 = boto3.resource(  # pylint: disable=C0103
            "s3",
            endpoint_url="http://127.0.0.1:9000",
            config=boto3.session.Config(signature_version="s3v4"),
            aws_access_key_id="miniouser",
            aws_secret_access_key="miniopass",
        )
        return s3.Bucket(self._configuration.bucket)

    def get_all_keys_for_backup(self) -> list[str]:
        self.logger.info("get_all_keys_for_backup(): getting list of files for %s", self._configuration.backup_name)
        all_files = list(self.s3_bucket.objects.filter(Prefix=f"{self._configuration.backup_name}/"))
        self.logger.info("get_all_keys_for_backup(): Done. Got %s files", len(all_files))
        return [_.key for _ in all_files]

    def get_parsed_keys_for_metadata(self) -> list[ParsedKey]:
        return [_ for _ in self.get_all_parsed_keys_for_backup_sorted() if _.is_metadata()]

    def get_parsed_keys_for_data(self) -> list[ParsedKey]:
        return [_ for _ in self.get_all_parsed_keys_for_backup_sorted() if _.is_data()]

    def get_all_parsed_keys_for_backup_sorted(self) -> list[ParsedKey]:
        all_keys = self.get_all_keys_for_backup()
        if not all_keys:
            return []

        all_maybe_parsed_keys = [self.parse_key(a_key) for a_key in all_keys]
        all_parsed_keys = [_ for _ in all_maybe_parsed_keys if _.parsed]
        random.shuffle(all_parsed_keys)  # FIXME: remove this trick and add testing
        all_parsed_keys = sorted(all_parsed_keys, key=lambda _: int(_.block_number))

        self.logger.info(
            "get_all_parsed_keys_for_backup_sorted(): parsed %s of %s", len(all_parsed_keys), len(all_keys)
        )
        self.logger.debug("get_all_parsed_keys_for_backup_sorted(): parsed: %s", all_parsed_keys)
        return all_parsed_keys


class Validator:
    logger = logging.getLogger(f"{__name__}.Validator")

    def __init__(self, configuration: Configuration):
        assert configuration
        self._configuration = configuration

    def _validate_single_block_data(self, block_data: BlockData, block_metadata: BlockMetadata):
        counter: dict[TopicPartition, int] = defaultdict(lambda: 0)
        for message in block_data.events:
            topic_partition = TopicPartition(
                topic=message[KAFKA_MESSAGE_TUPLE_TOPIC],  # type: ignore
                partition=message[KAFKA_MESSAGE_TUPLE_PARTITION],  # type: ignore
            )
            counter[topic_partition] += 1
            if topic_partition in block_metadata.start:
                assert message[KAFKA_MESSAGE_TUPLE_OFFSET] >= block_metadata.start[topic_partition]  # type: ignore

            assert topic_partition in block_metadata.end, "Event in BlockData not in BlockMetadata.end"
            assert message[KAFKA_MESSAGE_TUPLE_OFFSET] <= block_metadata.end[topic_partition]  # type: ignore

        assert counter == block_metadata.counters, "Counter in metadata don't match actual data"

        self.logger.info("Block %s -> _validate_single_block_data() OK", block_metadata.block_number)

    def _validate_single_block_metadata(self, block_metadata: BlockMetadata):
        # we should have less topic/partitions at `start` than at the `end`
        assert set(block_metadata.start.keys()) <= set(block_metadata.end.keys())

        # offsets at `start` should be <= compared to `end`
        for topic_partition, end_offset in block_metadata.end.items():
            if topic_partition in block_metadata.start:
                assert block_metadata.start[topic_partition] <= end_offset

        self.logger.info("Block %s -> _validate_single_block_metadata() = OK", block_metadata.block_number)

    def validate_single_block(self, block_data: BlockData, block_metadata: BlockMetadata):
        # we should have less topic/partitions at `start` than at the `end`
        assert set(block_metadata.start.keys()) <= set(block_metadata.end.keys())

        # offsets at `start` should be <= compared to `end`
        for topic_partition, end_offset in block_metadata.end.items():
            if topic_partition in block_metadata.start:
                assert block_metadata.start[topic_partition] <= end_offset

        self.logger.info("Block %s -> validate_single_block() = OK", block_metadata.block_number)

        self._validate_single_block_metadata(block_metadata)
        self._validate_single_block_data(block_data, block_metadata)

    def validate_consecutive_blocks_metadata(self, prev: BlockMetadata, cur: BlockMetadata):
        assert prev.block_number + 1 == cur.block_number, "Block are not consecutive"
        assert prev.end == cur.start, "End and start don't match"

        self.logger.info(
            "Block %s/%s -> validate_consecutive_blocks_metadata() = OK", prev.block_number, cur.block_number
        )


class S3BlockStorage(BlockStorage):
    logger = logging.getLogger(f"{__name__}.S3BlockStorage")

    def __init__(self, configuration: Configuration):
        super().__init__()
        self._configuration = configuration
        self._s3_client = S3Client(configuration=configuration)
        self._validator = Validator(configuration=configuration)
        self._pretty_printer = PrettyPrinter(configuration=configuration)

    def _upload(
        self, serialized_data: bytes, key_data: str, serialized_metadata: bytes, key_metadata: str
    ) -> BlockUploadResult:
        # This is not efficient at all, needs to be improved in future iterations
        self._s3_client.s3_bucket.upload_fileobj(io.BytesIO(serialized_data), key_data)
        self._s3_client.s3_bucket.upload_fileobj(io.BytesIO(serialized_metadata), key_metadata)

        return BlockUploadResult(
            data_path=f"s3://{self._configuration.bucket}/{key_data}",
            metadata_path=f"s3://{self._configuration.bucket}/{key_metadata}",
        )

    def serialize_and_upload(self, block: Block) -> BlockUploadResult:
        jitter = uuid4().hex  # TODO: would be much better to use block checksum

        key_data = S3Client.build_key(
            backup_name=self._configuration.backup_name,
            data_metadata="data",
            block_number=str(block.metadata.block_number),
            jitter=jitter,
            file_ext="pickle",
        )

        key_metadata = S3Client.build_key(
            backup_name=self._configuration.backup_name,
            data_metadata="metadata",
            block_number=str(block.metadata.block_number),
            jitter=jitter,
            file_ext="json",
        )

        serialized_data = pickle.dumps(block.data)
        serialized_metadata = json.dumps(block.metadata.as_dict()).encode("utf-8")

        return self._upload(serialized_data, key_data, serialized_metadata, key_metadata)

    def get_latest_block_metadata(self) -> BlockMetadata:
        parsed_keys = self._s3_client.get_parsed_keys_for_metadata()
        if not parsed_keys:
            raise BackupNotInitializedError()
        latest = max(parsed_keys, key=lambda _: int(_.block_number))  # type: ignore # FIXME!
        self.logger.debug("get_latest_block_metadata(): latest: %s", latest)
        return self.download_metadata(latest.key)

    def download_metadata(self, key: str) -> BlockMetadata:
        assert S3Client.parse_key(key).is_metadata()
        buffer = io.BytesIO()
        self._s3_client.s3_bucket.download_fileobj(key, buffer)
        deserialized_metadata_dict = json.loads(buffer.getvalue().decode("utf-8"))
        deserialized_metadata = BlockMetadata.from_dict(deserialized_metadata_dict)
        self.logger.debug("download_metadata(): deserialized_metadata: %s", deserialized_metadata)
        return deserialized_metadata

    def get_all_blocks_metadata(self) -> list[BlockMetadata]:
        return [self.download_metadata(_.key) for _ in self._s3_client.get_parsed_keys_for_metadata()]

    def iter_sorted_all_blocks_metadata(self) -> Iterator[BlockMetadata]:
        all_metadata = self._s3_client.get_parsed_keys_for_metadata()
        for _ in all_metadata:
            yield self.download_metadata(_.key)

    def download_data(self, block_metadata: BlockMetadata) -> BlockData:
        s3_prefix = f"{self._configuration.backup_name}/data-{block_metadata.block_number}-"
        backup_files = list(self._s3_client.s3_bucket.objects.filter(Prefix=s3_prefix))

        match len(backup_files):
            case 0:
                raise Exception(f"No data block found for metadata {block_metadata}")
            case 1:
                block_data_file = backup_files[0]
                self.logger.info("Block data found: %s", block_data_file)
            case _:
                raise Exception(f"More than one data block found: {backup_files}")

        buffer = io.BytesIO()
        self._s3_client.s3_bucket.download_fileobj(block_data_file.key, buffer)
        deserialized_data: BlockData = pickle.loads(buffer.getvalue())

        self.logger.debug(
            "Data block retrieved from %s. Dump: %s", block_data_file.key, pprint.pformat(deserialized_data)
        )

        return deserialized_data

    def validate(self):
        prev_metadata = None
        cur_metadata = None
        for metadata in self.iter_sorted_all_blocks_metadata():
            assert metadata is not None
            self.logger.info("Validating metadata: %s", self._pretty_printer.log_metadata(metadata))
            prev_metadata = cur_metadata
            cur_metadata = metadata
            data = self.download_data(cur_metadata)
            self.logger.info("Validating data: %s", self._pretty_printer.log_data(data))

            self._validator.validate_single_block(data, cur_metadata)
            if prev_metadata is not None:
                self._validator.validate_consecutive_blocks_metadata(prev_metadata, cur_metadata)
