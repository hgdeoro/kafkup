import os

from kafkup.block import TopicPartition


def get_partitions():
    print(f"Using TOPIC_PARTITIONS={os.environ['TOPIC_PARTITIONS']}")
    for topic_partition_str in os.environ["TOPIC_PARTITIONS"].strip().split(","):
        topic, partition = topic_partition_str.strip().rsplit(":", maxsplit=1)
        yield TopicPartition(topic=topic, partition=int(partition))
