import itertools
import logging
import multiprocessing
import os
import time
from uuid import uuid4

from confluent_kafka import Producer as _KafkaProducer
from confluent_kafka.admin import AdminClient as _KafkaAdminClient
from confluent_kafka.admin import NewTopic as _KafkaNewTopic

from kafkup.configuration import Configuration
from tests.kafka_mock import generate_random_big_binary_payload_to_produce
from tests.kafka_mock import generate_random_payload_to_produce

logger = logging.getLogger(__name__)


"""
Usage to use existing topic:

    env KAFKUP_LOGGING_LEVEL=info \
        TOPIC="topic-name" \
        KAFKUP_CONFIGURATION_FILE=config.json \
            python3 -m kafkup.cli.synthetic_create_and_populate

Usage to create new topic:

    env KAFKUP_LOGGING_LEVEL=info \
        KAFKUP_CONFIGURATION_FILE=config.json \
            python3 -m kafkup.cli.synthetic_create_and_populate
"""


def _get_topics(kafka_admin):
    return [_[0] for _ in kafka_admin.list_topics().topics.items()]


def main():
    configuration = Configuration.from_env()
    logging.basicConfig(level=configuration.logging_level)

    kafka_admin = _KafkaAdminClient(configuration.kafka_config)
    wait_between_produce = float(os.environ.get("WAIT_BETWEEN_PRODUCE", "0"))
    match os.environ.get("DATA_GENERATOR_FUNC"):
        case "generate_random_big_binary_payload_to_produce":
            data_generator_func = generate_random_big_binary_payload_to_produce
        case "generate_random_payload_to_produce":
            data_generator_func = generate_random_payload_to_produce
        case _:
            logger.warning("Ignoring invalid DATA_GENERATOR_FUNC %s", os.environ.get("DATA_GENERATOR_FUNC"))
            data_generator_func = generate_random_payload_to_produce

    # ---------- Create topic

    if "TOPIC" in os.environ:
        topic = os.environ["TOPIC"]
        logger.info("Using existing TOPIC: %s", topic)
    else:
        topic = f"test-{uuid4()}"
        config = {}
        logger.info("Creating new TOPIC: %s with config: %s", topic, config)
        ret = kafka_admin.create_topics([_KafkaNewTopic(topic=topic, num_partitions=3, config=config)])
        assert ret[topic].result() is None

        while True:
            if topic in _get_topics(kafka_admin):  # pylint: disable=R1723
                logger.info("New topic found, continuing... topic=%s", topic)
                break
            else:
                logger.info("New topic not found, will retry...")
                time.sleep(1.0)

    # ---------- Populate topic

    logger.info("Start population of topic %s...", topic)

    class ProducerProcess(multiprocessing.Process):
        def run(self):
            kafka_producer = _KafkaProducer(configuration.kafka_config)
            reporter = Reporter(thread_ident=self.ident)
            for num in itertools.count():
                payload = data_generator_func()
                kafka_producer.produce(topic=topic, callback=reporter.delivery_report, **payload)
                if num % 1000 == 0:
                    kafka_producer.flush()
                time.sleep(wait_between_produce)

    processes = [ProducerProcess(daemon=True) for _ in range(4)]
    for a_process in processes:
        a_process.start()
    for a_process in processes:
        a_process.join()


class Reporter:  # pylint: disable=too-few-public-methods
    def __init__(self, thread_ident):
        self.thread_ident = thread_ident
        self.start = time.monotonic()
        self.last_report = time.monotonic()
        self.count = 0

    def delivery_report(self, err, msg):
        if err:
            raise Exception(f"Message not delivered: {err}")

        self.count += 1
        now = time.monotonic()
        if self.last_report + 1 > now:
            logger.debug(
                "[t=%s] Message delivered. Total=%s. msg=%s, topic=%s, offset=%s",
                self.thread_ident,
                self.count,
                msg,
                msg.topic(),
                msg.offset(),
            )
        else:
            logger.info(
                "[t=%s] Message delivered. Total=%s. Throughput=%s/sec. msg=%s, topic=%s, offset=%s",
                self.thread_ident,
                self.count,
                int(self.count / (now - self.start)),
                msg,
                msg.topic(),
                msg.offset(),
            )
            self.last_report = now


if __name__ == "__main__":
    main()
