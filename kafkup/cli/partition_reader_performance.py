import logging
import os
import pprint
import threading
import time

from confluent_kafka import Message as KafkaMessage

from kafkup.block import Block
from kafkup.block import BlockMetadata
from kafkup.block_builder import BlockBuilder
from kafkup.block_builder import InMemoryBlockBuilder
from kafkup.block_storage import BackupNotInitializedError
from kafkup.block_storage import BlockStorage
from kafkup.block_storage import BlockUploadResult
from kafkup.block_storage import S3BlockStorage
from kafkup.cli import get_partitions
from kafkup.configuration import Configuration
from kafkup.partition_reader import PartitionReaderBase

# mypy: ignore-errors

# pylint: disable=pointless-string-statement
"""
Usage:

    env KAFKUP_LOGGING_LEVEL=info \
        TOPIC_PARTITIONS="topic:partition,topic:partition"
        KAFKUP_CONFIGURATION_FILE=config.json \
            python3 -m kafkup.cli.partition_reader_performance
"""


def format_bytes(size):
    power = 2**10  # 2**10 = 1024
    iters = 0
    power_labels = {0: "", 1: "KiB", 2: "MiB", 3: "GiB", 4: "TiB"}
    while size > power:
        size /= power
        iters += 1
    return f"{size:.1f} {power_labels[iters]}"


class NoOpBlockBuilder(BlockBuilder):  # pylint: disable=too-few-public-methods
    def __init__(self, configuration: Configuration, wrapped_builder: BlockBuilder | None = None):
        self._configuration = configuration
        self._message_count = 0
        self._accumulated_size = 0
        self._start_time = time.monotonic()
        self._wrapped_builder = wrapped_builder
        self._bytes_limit = self._get_bytes_limit()

    @classmethod
    def _get_bytes_limit(cls):
        if os.environ.get("BLOCK_BUILDER_BYTES_LIMIT"):
            return int(os.environ.get("BLOCK_BUILDER_BYTES_LIMIT"))
        return None

    def handle_event(self, kafka_message: KafkaMessage) -> Block | None:
        # Update metrics
        self._message_count += 1
        self._accumulated_size += len(kafka_message.topic())
        self._accumulated_size += len(kafka_message.key())
        self._accumulated_size += len(kafka_message.value())
        if kafka_message.headers():
            for header in kafka_message.headers():
                self._accumulated_size += len(header[0])
                self._accumulated_size += len(header[1])

        if self._message_count and self._message_count % 1_000 == 0:
            elapsed_time = time.monotonic() - self._start_time
            throughput_messages = int(self._message_count / elapsed_time)
            throughput_size = int(self._accumulated_size / elapsed_time)

            pretty_accumulated_size = format_bytes(self._accumulated_size)
            pretty_throughput_size = format_bytes(throughput_size)

            print(
                f"Status \t"
                f"messages={self._message_count}\t"
                f"size={pretty_accumulated_size}\t"
                f"time={elapsed_time:.1f}\t"
                f"throughput={throughput_messages} msg/sec\t"
                f"throughput={pretty_throughput_size}/sec"
            )

            if self._bytes_limit is not None and self._accumulated_size > self._bytes_limit:
                raise StopIteration()

        if self._wrapped_builder:
            block = self._wrapped_builder.handle_event(kafka_message)
            if block:
                print(f"Block created: {block.metadata.block_number}")
                return block

        return None


class NoOpBlockStorage(BlockStorage):
    def __init__(self, configuration: Configuration):
        self._configuration = configuration

    def serialize_and_upload(self, block: Block) -> BlockUploadResult:
        return BlockUploadResult(data_path="s3://bla/bla", metadata_path="s3://bla/bla")

    def get_all_blocks_metadata(self) -> list[BlockMetadata]:
        return []

    def get_latest_block_metadata(self) -> BlockMetadata:
        raise BackupNotInitializedError()

    def validate(self):
        pass


class PatchedS3BlockStorage(S3BlockStorage):
    def _upload(
        self, serialized_data: bytes, key_data: str, serialized_metadata: bytes, key_metadata: str
    ) -> BlockUploadResult:
        return BlockUploadResult(
            data_path=f"s3://{self._configuration.bucket}/{key_data}",
            metadata_path=f"s3://{self._configuration.bucket}/{key_metadata}",
        )

    def get_all_blocks_metadata(self) -> list[BlockMetadata]:
        return []

    def get_latest_block_metadata(self) -> BlockMetadata:
        raise BackupNotInitializedError()

    def validate(self):
        pass


def start_read_queue_thread(result_queue, signal_queue):
    def func():
        logging.info("Waiting for result...")
        while True:
            logging.info("Waiting for result...")
            result: dict = result_queue.get()
            logging.debug("Got result: %s", pprint.pformat(result))

    logging.info("Using result_queue=%s, signal_queue=%s", result_queue, signal_queue)
    thread = threading.Thread(daemon=True, target=func)
    thread.start()
    return thread


def main():
    configuration = Configuration.from_env()
    logging.basicConfig(level=configuration.logging_level)

    match os.environ.get("PERFORMANCE_TEST", "consumption").lower():
        case "full":
            print("PERFORMANCE_TEST=full: using S3BlockStorage, NoOpBlockBuilder+InMemoryBlockBuilder")
            block_storage_impl = S3BlockStorage(configuration=configuration)
            block_builder = NoOpBlockBuilder(
                configuration=configuration, wrapped_builder=InMemoryBlockBuilder(configuration=configuration)
            )
        case "serialization":
            print("PERFORMANCE_TEST=serialization: using PatchedS3BlockStorage, NoOpBlockBuilder+InMemoryBlockBuilder")
            block_storage_impl = PatchedS3BlockStorage(configuration=configuration)
            block_builder = NoOpBlockBuilder(
                configuration=configuration, wrapped_builder=InMemoryBlockBuilder(configuration=configuration)
            )
        case "consumption":
            print("PERFORMANCE_TEST=consumption: using NoOpBlockStorage, NoOpBlockBuilder")
            block_storage_impl = NoOpBlockStorage(configuration=configuration)
            block_builder = NoOpBlockBuilder(configuration=configuration, wrapped_builder=None)
        case _:
            raise Exception("Invalid PERFORMANCE_TEST")

    partition_reader_process = PartitionReaderBase.as_process()(
        daemon=True,
        partitions=list(get_partitions()),
        configuration=configuration,
        block_builder=block_builder,
        block_storage=block_storage_impl,
    )
    result_queue, signal_queue = partition_reader_process.for_thread()
    thread = start_read_queue_thread(result_queue, signal_queue)  # noqa: F841 pylint: disable=unused-variable

    partition_reader_process.run()


if __name__ == "__main__":
    main()
