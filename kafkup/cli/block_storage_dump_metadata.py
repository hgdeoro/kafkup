import logging
import pprint

from kafkup.block import BlockMetadata
from kafkup.block_storage import S3BlockStorage
from kafkup.configuration import Configuration

# mypy: ignore-errors

# pylint: disable=pointless-string-statement
"""
Usage:

    env KAFKUP_LOGGING_LEVEL=info \
        KAFKUP_BACKUP_NAME=... \
        KAFKUP_CONFIGURATION_FILE=config.json \
            python3 -m kafkup.cli.block_storage_dump_metadata
"""


def main():
    configuration = Configuration.from_env()
    logging.basicConfig(level=configuration.logging_level)
    storage = S3BlockStorage(configuration=configuration)
    blocks_metadata: list[BlockMetadata] = storage.get_all_blocks_metadata()
    print(f"Got {len(blocks_metadata)} blocks metadata for backup '{configuration.backup_name}'")

    for metadata in blocks_metadata:
        print(f"----- Block {metadata.block_number}")
        print(pprint.pformat(metadata))


if __name__ == "__main__":
    main()
