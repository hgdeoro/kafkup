import logging
import pprint

from kafkup.block_builder import InMemoryBlockBuilder
from kafkup.block_storage import S3BlockStorage
from kafkup.cli import get_partitions
from kafkup.configuration import Configuration
from kafkup.partition_reader import PartitionReaderBase

# mypy: ignore-errors

# pylint: disable=pointless-string-statement
"""
Usage:

    env KAFKUP_LOGGING_LEVEL=info \
        KAFKUP_BACKUP_NAME=backup-from-cli-$(date +%s) \
        TOPIC_PARTITIONS="topic:partition,topic:partition"
        KAFKUP_CONFIGURATION_FILE=config.json \
            python3 -m kafkup.cli.partition_reader_backup_topics
"""


def main():
    configuration = Configuration.from_env()
    print(f"Uploading to BACKUP_NAME: {configuration.backup_name}")
    logging.basicConfig(level=configuration.logging_level)
    process = PartitionReaderBase.as_process()(
        daemon=True,
        partitions=list(get_partitions()),
        configuration=configuration,
        block_builder=InMemoryBlockBuilder(configuration=configuration),
        block_storage=S3BlockStorage(configuration=configuration),
    )
    process.start()

    while True:
        logging.info("Waiting for result...")
        result: dict = process.result_queue.get()
        logging.debug("Got result: %s", pprint.pformat(result))


if __name__ == "__main__":
    main()
