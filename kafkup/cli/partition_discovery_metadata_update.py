import logging
import os
import pprint

from kafkup.configuration import Configuration
from kafkup.partition_discovery import MetadataUpdate
from kafkup.partition_discovery import PartitionDiscoveryBase

# mypy: ignore-errors

# pylint: disable=pointless-string-statement
"""
Usage:

    env KAFKUP_LOGGING_LEVEL=info \
        KAFKUP_BACKUP_NAME=ignore-this \
        KAFKUP_CONFIGURATION_FILE=config.json \
            python3 -m kafkup.cli.partition_discovery_metadata_update
"""


def main():
    configuration = Configuration.from_env()
    logging.basicConfig(level=configuration.logging_level)

    process = PartitionDiscoveryBase.as_thread()(
        daemon=True,
        sleep_between_poll=int(os.environ.get("SLEEP_BETWEEN_POLL", "1")),
        configuration=configuration,
    )
    process.start()
    while True:
        update: MetadataUpdate = process.metadata_update_queue.get()
        if update.changed:
            print("#")
            print("# metadata changed")
            print("#")
            print(pprint.pformat(update))


if __name__ == "__main__":
    main()
