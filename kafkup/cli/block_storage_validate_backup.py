import logging

from kafkup.block_storage import S3BlockStorage
from kafkup.configuration import Configuration

# pylint: disable=pointless-string-statement
"""
Usage:

    env KAFKUP_LOGGING_LEVEL=info \
        KAFKUP_BACKUP_NAME=... \
        KAFKUP_CONFIGURATION_FILE=config.json \
            python3 -m kafkup.cli.block_storage_validate_backup
"""


def main():
    configuration = Configuration.from_env()
    logging.basicConfig(level=configuration.logging_level)
    storage = S3BlockStorage(configuration=configuration)
    storage.validate()


if __name__ == "__main__":
    main()
