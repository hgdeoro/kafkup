from __future__ import annotations

import abc
import multiprocessing
import queue
import threading
from typing import Self
from typing import TypeAlias
from typing import Union

XQueue: TypeAlias = Union[multiprocessing.Queue, queue.Queue]


class ThreadOrProcess(abc.ABC):
    @abc.abstractmethod
    def _new_queue(self, maxsize=0) -> XQueue:
        ...

    @abc.abstractmethod
    def identifier(self) -> int | None:
        """Returns the identifier for the process or thread"""
        ...

    def start(self):
        return super().start()

    def join(self, timeout=None):
        return super().join(timeout)

    def is_alive(self):
        return super().is_alive()

    @classmethod
    def as_thread(cls) -> type[Self]:
        return type(
            f"{getattr(cls, 'SUBCLASS_BASENAME')}Thread",
            (
                cls,
                ThreadMixin,
            ),
            {},
        )

    @classmethod
    def as_process(cls) -> type[Self]:
        return type(
            f"{getattr(cls, 'SUBCLASS_BASENAME')}Process",
            (
                cls,
                ProcessMixin,
            ),
            {},
        )


class ThreadMixin(threading.Thread, ThreadOrProcess):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _new_queue(self, maxsize=0) -> XQueue:
        return queue.Queue(maxsize=maxsize)

    def identifier(self):
        return self.ident


class ProcessMixin(multiprocessing.Process, ThreadOrProcess):
    def _new_queue(self, maxsize=0) -> XQueue:
        return multiprocessing.Queue(maxsize=maxsize)

    def identifier(self):
        return self.pid
