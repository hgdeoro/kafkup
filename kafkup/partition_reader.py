import abc
import logging
import queue
import time

from kafkup import kafka_client
from kafkup.block import Block
from kafkup.block import TopicPartition
from kafkup.block_builder import BlockBuilder
from kafkup.block_storage import BackupNotInitializedError
from kafkup.block_storage import BlockStorage
from kafkup.concurrency import ThreadOrProcess
from kafkup.concurrency import XQueue
from kafkup.configuration import Configuration
from kafkup.pretty_print import PrettyPrinter

SIGNAL_ABORT = "ABORT"

STATUS_NEW_BLOCK_FINISHED = "NEW_BLOCK_FINISHED"
STATUS_ERROR = "ERROR"
STATUS_ABORTED = "ABORTED"
STATUS_BLOCK_UPLOADED = "BLOCK_UPLOADED"


class StopProcessException(Exception):
    pass


class AbortProcessException(Exception):
    pass


class PartitionReaderBase(ThreadOrProcess, abc.ABC):  # pylint: disable=too-many-instance-attributes
    SUBCLASS_BASENAME = "PartitionReaderBase"
    logger = logging.getLogger(f"{__name__}.PartitionReaderBase")

    def __init__(
        self,
        partitions: list[TopicPartition],
        block_builder: BlockBuilder,
        block_storage: BlockStorage,
        configuration: Configuration,
        *args,
        **kwargs,
    ):
        assert partitions is not None
        assert block_builder is not None
        assert block_storage is not None
        assert configuration
        for _ in partitions:
            if not isinstance(_, TopicPartition):
                raise Exception(f"Not a 'TopicPartition': type={type(_)}, instance={_}")
        super().__init__(*args, **kwargs)
        self._partitions: list[TopicPartition] = partitions
        self._result_queue: XQueue = self._new_queue()
        self._signal_queue: XQueue = self._new_queue()
        self._kafka_client = kafka_client.KafkaClient(configuration)
        self._block_builder: BlockBuilder = block_builder
        self._block_storage: BlockStorage = block_storage
        self._configuration: Configuration = configuration
        self._pretty_printer = PrettyPrinter(configuration=configuration)

    @property
    def result_queue(self):
        return self._result_queue

    @property
    def signal_queue(self):
        return self._signal_queue

    def for_thread(self) -> tuple[queue.Queue, queue.Queue]:
        self._result_queue = queue.Queue()
        self._signal_queue = queue.Queue()
        return self._result_queue, self._signal_queue

    def run(self) -> None:
        try:
            self._run()
        except StopProcessException:
            logging.warning("StopProcessException received. Exiting...")
            return
        except AbortProcessException:
            logging.warning("AbortProcessException received. Exiting...")
            return
        except Exception as err:  # pylint: disable=broad-exception-caught
            logging.exception("Exception detected. Exiting...")
            self._result_queue.put({"status": STATUS_ERROR, "error": str(err)})
            return

    def _on_new_block_finished(self, block: Block):
        self._result_queue.put({"status": STATUS_NEW_BLOCK_FINISHED, "metadata": block.metadata})
        upload_result = self._block_storage.serialize_and_upload(block)
        self._result_queue.put(
            {"status": STATUS_BLOCK_UPLOADED, "metadata": block.metadata, "upload_result": upload_result}
        )

    def _on_abort(self):
        self.logger.info("Aborting.")
        self._result_queue.put({"status": STATUS_ABORTED})
        raise AbortProcessException()

    def _check_signals(self):
        try:
            received_signal = self._signal_queue.get(block=False)
        except queue.Empty:
            return

        self.logger.info("Received signal=%s", received_signal)
        match received_signal:
            case "ABORT":
                self._on_abort()
            case _:
                raise Exception(f"Invalid signal: '{received_signal}'")

    def _run(self) -> None:
        if not self._partitions:
            while True:
                self._check_signals()
                self.logger.info("No partitions to backup... Sleeping...")
                time.sleep(1.0)
        try:
            last_block_metadata = self._block_storage.get_latest_block_metadata()
            self.logger.info("Got last_block_metadata: %s", last_block_metadata)
        except BackupNotInitializedError:
            self.logger.info("There is no last_block_metadata")
            last_block_metadata = None

        offsets = self._kafka_client.build_assign_partitions_list(self._partitions, last_block_metadata)
        self.logger.info("Will assign consumer to offsets: %s", offsets)
        consumer = self._kafka_client.get_consumer_for_partitions(offsets)
        cur_check = time.monotonic()
        last_check = cur_check
        while True:
            event = consumer.poll(0.1)
            if event is None:
                self.logger.debug("poll() timeout: no event consumed")
            else:
                new_block = self._block_builder.handle_event(event)
                if new_block is not None:
                    self.logger.info("New block finished: %s", self._pretty_printer.log_metadata(new_block.metadata))
                    self._on_new_block_finished(new_block)

            cur_check = time.monotonic()
            if cur_check - last_check >= 1.0:
                last_check = cur_check
                self._check_signals()
