import collections
import dataclasses
import logging
from functools import cached_property

from confluent_kafka import OFFSET_BEGINNING
from confluent_kafka import Consumer as _KafkaConsumer
from confluent_kafka import TopicPartition as _KafkaTopicPartition
from confluent_kafka.admin import AdminClient as _KafkaAdminClient
from confluent_kafka.admin import ClusterMetadata as _KafkaClusterMetadata

from kafkup.block import BlockMetadata
from kafkup.block import PartitionOffset
from kafkup.block import TopicPartition
from kafkup.configuration import Configuration

__all__ = [
    "PartitionMetadata",
    "TopicMetadata",
    "TopicsMetadata",
    "KafkaClient",
]


@dataclasses.dataclass(frozen=True)
class PartitionMetadata:
    topic: str
    partition: int
    offset: int | None


@dataclasses.dataclass(frozen=True)
class TopicMetadata:
    topic: str
    partitions_metadata: list[PartitionMetadata]


@dataclasses.dataclass(frozen=True)
class TopicsMetadata:
    topics: list[TopicMetadata]

    def get_partitions_metadata(self) -> list[PartitionMetadata]:
        return [pm for tm in self.topics for pm in tm.partitions_metadata]


def beginning_offset() -> int:
    return OFFSET_BEGINNING


class KafkaClient:
    logger = logging.getLogger(f"{__name__}.KafkaClient")

    def __init__(self, configuration: Configuration):
        assert configuration is not None
        self._configuration: Configuration = configuration

    @cached_property
    def _admin(self):
        return _KafkaAdminClient(self._configuration.kafka_config)

    def get_topics_metadata(self) -> TopicsMetadata:
        cluster_metadata: _KafkaClusterMetadata = self._admin.list_topics()
        return TopicsMetadata(
            topics=[
                TopicMetadata(
                    topic=topic,
                    partitions_metadata=[
                        PartitionMetadata(topic=topic, partition=partition, offset=None)
                        for partition, partition_metadata in topic_metadata.partitions.items()
                    ],
                )
                for topic, topic_metadata in cluster_metadata.topics.items()
                if not topic.startswith("__")
            ]
        )

    @classmethod
    def build_assign_partitions_list(
        cls, partitions: list[TopicPartition], last_block_metadata: BlockMetadata | None
    ) -> list[PartitionOffset]:
        assign_partitions: list[PartitionOffset] = []

        start_offsets: dict[TopicPartition, int] = collections.defaultdict(beginning_offset)
        if last_block_metadata is not None:
            start_offsets.update(last_block_metadata.end)

        for partition_metadata in partitions:
            topic = partition_metadata.topic
            partition = partition_metadata.partition
            offset = start_offsets[TopicPartition(topic=topic, partition=partition)]
            assign_partitions.append(
                PartitionOffset(
                    topic=topic,
                    partition=partition,
                    offset=offset,
                )
            )
            cls.logger.info("build_assign_partitions_list() topic=%s partition=%s offset=%s", topic, partition, offset)

        return assign_partitions

    def get_consumer_for_partitions(self, partitions: list[PartitionOffset]) -> _KafkaConsumer:
        assert None not in {_.offset for _ in partitions}, "PartitionOffset with offset None"
        kafka_consumer = _KafkaConsumer(self._configuration.kafka_config)
        kafka_consumer.assign(
            [_KafkaTopicPartition(topic=_.topic, partition=_.partition, offset=_.offset) for _ in partitions]
        )
        # TODO: does this fails if we don't have permissions to consume from topic?
        # TODO: can we use `kafka_consumer.position()` to check permissions, etc?
        return kafka_consumer
