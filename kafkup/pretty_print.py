import json

from prettytable import PrettyTable

from kafkup.block import KAFKA_MESSAGE_TUPLE_KEY
from kafkup.block import KAFKA_MESSAGE_TUPLE_OFFSET
from kafkup.block import KAFKA_MESSAGE_TUPLE_PARTITION
from kafkup.block import KAFKA_MESSAGE_TUPLE_TOPIC
from kafkup.block import KAFKA_MESSAGE_TUPLE_VALUE
from kafkup.block import BlockData
from kafkup.block import BlockMetadata
from kafkup.block import message_tuple_to_dict
from kafkup.configuration import Configuration

# mypy: ignore-errors


class PrettyPrinter:
    def __init__(self, configuration: Configuration):
        assert configuration
        self._configuration = configuration

    def _safe(self, value: str | bytes) -> str:
        if isinstance(value, str):  # pylint: disable=no-else-return
            return value[:30]
        elif isinstance(value, bytes):
            return value.decode("ascii")[:30]  # FIXME: this won't work
        else:
            raise Exception(f"Don't know what to do with value of type {type(value)}")

    def log_data(self, data: BlockData) -> BlockData | str:
        # pylint: disable=no-else-return
        if self._configuration.pretty_print_level == Configuration.PRETTY_PRINT_LEVEL_DISABLED:
            return data
        else:
            return "\n" + self.pretty_print_data(data)

    def log_metadata(self, metadata: BlockMetadata) -> BlockMetadata | str:
        #  pylint: disable=no-else-return
        if self._configuration.pretty_print_level == Configuration.PRETTY_PRINT_LEVEL_DISABLED:
            return metadata
        else:
            return "\n" + self.pretty_print_metadata(metadata)

    def pretty_print_data(self, data: BlockData) -> str:
        table_block = PrettyTable()
        table_block.field_names = ["Name", "Value"]
        table_block.add_row(["# messages in block", len(data.events)])
        table_block.align = "r"
        table_block.align["Name"] = "l"

        table_partitions = PrettyTable()
        table_partitions.field_names = ["Topic", "Partition", "Offset", "Key", "Value", "Headers"]
        for event in data.events:
            table_partitions.add_row(
                [
                    event[KAFKA_MESSAGE_TUPLE_TOPIC],
                    str(event[KAFKA_MESSAGE_TUPLE_PARTITION]),
                    event[KAFKA_MESSAGE_TUPLE_OFFSET],
                    self._safe(event[KAFKA_MESSAGE_TUPLE_KEY]),
                    self._safe(event[KAFKA_MESSAGE_TUPLE_VALUE]),
                    "-",
                ]
            )
        table_partitions.align = "r"
        table_partitions.align["Topic"] = "l"
        table_partitions.align["Key"] = "l"
        table_partitions.align["Value"] = "l"
        table_partitions.align["Headers"] = "l"

        # If `PRETTY_PRINT_LEVEL_DETAILED`, include details
        if self._configuration.pretty_print_level == Configuration.PRETTY_PRINT_LEVEL_DETAILED:

            def serializer_default(instance):
                return f"<object of type {type(instance)}>"

            json_dump = json.dumps(
                [message_tuple_to_dict(_) for _ in data.events], sort_keys=True, indent=4, default=serializer_default
            )
            return table_block.get_string() + "\n" + table_partitions.get_string() + "\n" + json_dump

        # If `PRETTY_PRINT_LEVEL_DISABLED` or `PRETTY_PRINT_LEVEL_ENABLED`, print normally
        return table_block.get_string() + "\n" + table_partitions.get_string()

    def pretty_print_metadata(self, metadata: BlockMetadata) -> str:
        table_block = PrettyTable()
        table_block.field_names = ["Name", "Value"]
        table_block.add_row(["Block number", metadata.block_number])
        table_block.add_row(["# messages in block", sum(metadata.counters.values())])
        table_block.add_row(["# partitions @ start", len(metadata.start)])
        table_block.add_row(["# partitions @ end", len(metadata.end)])
        table_block.align = "r"
        table_block.align["Name"] = "l"

        table_partitions = PrettyTable()
        table_partitions.field_names = ["Topic", "Partition", "Start", "End", "Count"]
        for topic_partition in sorted(list(metadata.end.keys()), key=lambda _: (_.topic, _.partition)):
            topic = topic_partition.topic
            partition = topic_partition.partition
            offset_start = metadata.start[topic_partition] if topic_partition in metadata.start else ""
            offset_end = metadata.end[topic_partition]
            count = metadata.counters[topic_partition] if topic_partition in metadata.counters else ""
            table_partitions.add_row([topic, str(partition), offset_start, offset_end, count])
        table_partitions.align = "r"
        table_partitions.align["Topic"] = "l"

        # If `PRETTY_PRINT_LEVEL_DETAILED`, include details
        if self._configuration.pretty_print_level == Configuration.PRETTY_PRINT_LEVEL_DETAILED:
            json_dump = json.dumps(metadata.as_dict(), sort_keys=True, indent=4)
            return table_block.get_string() + "\n" + table_partitions.get_string() + "\n" + json_dump

        # If `PRETTY_PRINT_LEVEL_DISABLED` or `PRETTY_PRINT_LEVEL_ENABLED`, print normally
        return table_block.get_string() + "\n" + table_partitions.get_string()
