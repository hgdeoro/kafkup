from __future__ import annotations

import abc
import copy
import dataclasses
import logging
from collections import defaultdict

from confluent_kafka import Message as KafkaMessage

from kafkup.block import Block
from kafkup.block import BlockData
from kafkup.block import BlockMetadata
from kafkup.block import KafkaMessageTuple
from kafkup.block import TopicPartition
from kafkup.configuration import Configuration


class BlockBuilder(abc.ABC):  # pylint: disable=too-few-public-methods
    @abc.abstractmethod
    def handle_event(self, kafka_message: KafkaMessage) -> Block | None:
        ...


@dataclasses.dataclass
class IncrementalBlockMetadata:
    block_number: int
    start: dict[tuple[str, int], int]
    end: dict[tuple[str, int], int]
    counters: dict[tuple[str, int], int]

    @classmethod
    def new_root_block(cls):
        return IncrementalBlockMetadata(start={}, end={}, counters=defaultdict(lambda: 0), block_number=0)

    def update(self, topic: str, partition: int, offset: int):
        key = (topic, partition)
        # update `end`
        if key in self.end:
            assert self.end[key] < offset
        self.end[key] = offset

        # update `counters`
        self.counters[key] += 1

    def get_metadata(self) -> BlockMetadata:
        return BlockMetadata(
            block_number=self.block_number,
            start={TopicPartition(*key): value for key, value in self.start.items()},
            end={TopicPartition(*key): value for key, value in self.end.items()},
            counters=copy.deepcopy({TopicPartition(*key): value for key, value in self.counters.items()}),
        )

    def get_next(self) -> IncrementalBlockMetadata:
        return IncrementalBlockMetadata(
            block_number=self.block_number + 1,
            start=copy.deepcopy(self.end),
            end=copy.deepcopy(self.end),
            counters=defaultdict(lambda: 0),
        )


class InMemoryBlockBuilder(BlockBuilder):  # pylint: disable=too-few-public-methods
    # FIXME: name should reflect that `number of messages` is used to decide when the block is ready
    logger = logging.getLogger(f"{__name__}.InMemoryBlockBuilder")

    def __init__(self, configuration: Configuration):
        self._configuration = configuration
        self._buffer: list[KafkaMessageTuple] = []
        self._incremental_metadata = IncrementalBlockMetadata.new_root_block()

    def handle_event(self, kafka_message: KafkaMessage) -> Block | None:
        """Handles a single message. Returns a Block if a block is ready, or None if not ready."""
        if kafka_message.error():
            # FIXME: implement error handling
            raise Exception(f"Consumed event is marked as error: {kafka_message.error()}")

        self._buffer.append(
            (
                kafka_message.topic(),
                kafka_message.partition(),
                kafka_message.offset(),
                kafka_message.timestamp(),
                kafka_message.key(),
                kafka_message.value(),
                kafka_message.headers(),
            )
        )
        self._incremental_metadata.update(
            topic=kafka_message.topic(), partition=kafka_message.partition(), offset=kafka_message.offset()
        )

        if len(self._buffer) >= self._configuration.max_messages_per_block:
            block = Block(metadata=self._incremental_metadata.get_metadata(), data=BlockData(events=self._buffer))
            self._incremental_metadata = self._incremental_metadata.get_next()
            self._buffer = []
            return block

        return None
